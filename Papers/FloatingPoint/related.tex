%!TEX root =  pap.tex

\section{Related Work}
\label{section:related}

The earliest formalizations of floating-point~\cite{Brown:1981:SBR:355972.355975} were limited by the diversity of
floating-point formats and systems they had to cover.  For example,
the formalization needed to support a range of bases, as 2, 16 and 10
were all in use.  Overflow and underflow were particularly problematic
as, again, systems in common usage took very different approaches to
handling them.  
\begin{longv}
One interesting note is that the motivation behind
this early work, to support writing portable numerical software, was also
one of the drivers of the development of \fpstd.
\end{longv}
The first formalization of \fpstd~\cite{Barrett:1989:FMA:65464.65476}
is notable on several grounds.  It was the first to use a formal
language (Z) and to be used to verify algorithms for the basic
operations.  The verification was manual, using Hoare logic, and the
algorithms in question were those implemented in the firmware of the T800 Transputer.
During the formalization, a few issues in \fpstd\ were found and
the verification uncovered bugs that would have been difficult to find
with testing~\cite{Woodcock:2009:FMP:1592434.1592436}.  
Foreshadowing the issue in the Pentium 1, bugs were found in the
Transputer's handling of floating-point, introduced by the translation
to machine code and manual ``tidying up''~\cite{Gibbons93formalmethods},
suggesting a need to extend the proof chain to the whole development
process and a need for greater automation.

The \texttt{FDIV} bug in the Pentium 1 and the cost of the resultant
recall spurred the use of machine-checked formal proofs in the design
of floating-point hardware~\cite{H06,H07}.  To this end, \fpstd\ was
formalized in a variety of interactive theorem provers, including 
Isabelle~\cite{IEEE_Floating_Point-AFP}, HOL~\cite{C95}, HOL Light
\cite{HOLLight} (used by Intel), ACL2~\cite{713311} (used by AMD and
Centaur), PVS~\cite{PaulS.:1995:DIF:886673} and Coq
\cite{coq1,Melquiond:2012:FAC:2228637.2228930,boldo:inria-00534854}. %,boldo:hal-00743090}.  
These and related
approaches~\cite{ayadMarche} share a number of common characteristics
due to the provers they targeted.  They are all instances of the axiomatic approach
described in Section~\ref{section:formalization};
generally reduce floating-point numbers 
to integers and reals; are intended for use in
machine checked proofs; and are normally used to verify
implementations of floating-point and specific algorithms
based on them.   In contrast, this work (and its precursor
\cite{RW10}) follows the algebraic approach; builds on computationally simple
primitives; and is intended to be a formal reference
for automatic theorem provers providing built-in support
for reasoning about floating-point arithmetic.

%\MBComment{I had originally planned to say something about decision
%  procedures and possibly something about SMT-LIB but I think the
%  space may be better used elsewhere.}

% If we do then Thomas suggests:
%
% Here is something that we perhaps should include in the related work. A 
% distinct non-feature of that work (and some others) is that they don't 
% handle NaNs and infinities, arguing you don't need to because of some 
% algebraic result of Ayad and Marche. In contrast we spend so much time 
% on precisely those, and in fact state at the beginning that this is one 
% reason for the "blow-up" of the theory. Would have been a nice 
% discussion point.

A number of SMT solvers provide support for early versions of our theory
by encoding floating-point expressions as bit-vector expressions 
based on the circuits used to implement floating-point operations. 
To improve performance, they often rely on over and under
approximation schemes.  
To our knowledge, the earliest implementation of this approach was 
given in the CBMC model checker~\cite{BKW09}.
The approach
%
is now used in Z3~\cite{Z3}, MathSAT~\cite{mathsat5}, SONOLAR
\cite{jan11:_autom_test_case_gener_with} and CVC4~\cite{Barrett:2011:CVC:2032305.2032319}, and improving it remains
an active area of research~\cite{DBLP:conf/cade/ZeljicWR14}.  
An alternative approach is based on abstract interpretation.
It uses intervals or other abstract domains
to over-approximate possible models, and
a system of branching and learning similar to the SAT algorithm CDCL
to narrow these to particular concrete models~\cite{haller:_decid_float_point_logic_with_system_abstr,brain13:_inter_based_verif_float_point}.  
There has also been work to integrate the automated prover Gappa~\cite{dinechin11:_certif_gappa} into SMT solvers~\cite{EasyChair:256},
although these solvers are not known to implement the semantics presented in
this paper.


