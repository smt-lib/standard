%!TEX root =  pap.tex

\section{Formal Foundations}
\label{section:formal-foundations}

\fpstd\ gives an informal definition of the semantics 
of floating-point operations:

\begin{quote}
\begin{shortv}
\small
\end{shortv}
Each of the computational operations that return a numeric result
specified by this standard shall be performed as if it first produced
an intermediate result correct to infinite precision and with
unbounded range, and then rounded that intermediate result, if
necessary, to fit in the destination's format.
\end{quote}

\noindent To formalize this it is helpful to define
an extension of the real numbers so that we can treat floating-point
values as if they had ``infinite precision and unbounded range,'' and to
define a notion of rounding.
Note that the extended reals are used here simply to aid 
the formal definition of floating-point operations.
They are not the domain of interpretation of floating-point numbers.
For that we will define a family of algebras over bit vector triples.

\subsection{Extended Reals}

We extend the set $\R$ of real numbers with three new elements:
$\erpinf$, $\erninf$ and $\ernan$,
which represent respectively positive and negative infinity and a special 
\emph{not a number} value used to obtain an arithmetically closed system.  
For each set $S \subseteq \R$ we can define the following sets:
\[
\begin{array}{l@{\qquad\quad}l}
  \affineExtend{S} ~=~ S \union \set{\erpinf, \erninf} &
  \closedAffineExtend{S}  ~=~ \affineExtend{S} \union \set{\ernan}
\end{array}
\]
When $S$ is the base of an ordered additive or multiplicative monoid or group, 
we extend $\R$'s binary operations $\erplus$ and $\ermult$, unary operations
$\erneg$ and $\errecip{(\_)}$ and order relation $\leq$.
Table \ref{table:extended-real-nan} gives axioms defining these operations
when one argument is $\ernan$.
Table \ref{table:extended-real-inf} gives axioms defining these operations 
when one argument is $\erpinf$ or
$\erninf$, as well as the axiom for the inverse of zero.
\begin{longv}
We remark that, 
although these definitions extend the operations of ordered fields and rings 
and can be applied to $\R$ and $\Z$, the extended reals $\eR$ and extended 
integers $\eZ$ do not have all of the structure of $\R$ and $\Z$.\observation{
$\eR$ is neither an additive or multiplicative group as
$(\erneg \ernan) \erplus \ernan \not\ereq 0$ and
$\errecip{\ernan} \ermult \ernan \not\ereq 1$, nor does it have an
  annihilating 0 as $\ernan \ermult 0 \not\ereq 0$.  However it is an
  additive and multiplicative commutative monoid with the
  distributivity property, a structure some authors refer to as
  a semi-ring.
}
\end{longv}

Note that $(\eR,\erlte)$ is a partial order since $\ernan$ is comparable only
with itself. 
In contrast $(\aeR,\erlte)$ is a total order.
%Note that $\ereq$ is set-wise equality, thus $\ernan = \ernan$.
The extended reals operate as three largely algebraically
independent subsets: 
$\set{\ernan}$, $\set{\erpinf, \erninf}$ and $\R$.
If a sub-expression of an expression $e$ evaluates to $\ernan$, 
then the whole $e$ evaluates to $\ernan$ --- the set is
closed under the basic operations. 
Infinities generate infinities or $\ernan$, although the reciprocal operator 
maps an infinity back to a real value.  
Reals are of course closed under all operations except reciprocal of zero.  
For convenience, we will also use the usual additional symbols 
in Table~\ref{table:defined-symbols},
which are definable in terms of the basic operations.

\begin{table}
\caption{
%  Axioms describing the behavior of $\ernan$ with $u \in \closedAffineExtend{S}$ 
  formalization of $\ernan$'s behavior, with $u \in \closedAffineExtend{S}$
}  
\label{table:extended-real-nan}  
\centering
\(
  \begin{array}{lllll@{\qquad\qquad}lll}
    u \erplus \ernan & = & \ernan \erplus u & = & \ernan & \erneg
    \ernan    & =
 & \ernan \\
    u \ermult \ernan & = & \ernan \ermult u & = & \ernan & \errecip{\ernan} & = & \ernan
  \end{array}
\)
\medskip

\(
  \begin{array}{lll@{\qquad\qquad}lll}
  \ernan \erlte u & \ifandonlyif & u = \ernan &
    u \erlte \ernan & \ifandonlyif & u = \ernan
  \end{array}
\)
\end{table}


\begin{table}
\caption{Defined symbols, with $x,y \in \closedAffineExtend{S}$}
\label{table:defined-symbols}
\centering
\(
  \begin{array}{rcl@{\qquad\qquad}rcl}
               &   &                       &
    x \ergte y & := & y \erlte x \\
    x \ersub y & := & x \erplus (\erneg y)  &
    x \erlt y & := & (x \erlte y) \land \lnot (x \ereq y) \\
    x \erdiv y & := & x \ermult \errecip{y} &
    x \ergt y & := & (x \ergte y) \land \lnot (x \ereq y)
%  \erabs{x} & = & 
%  \begin{cases}
%    x & 0 \lte x \\
%    -x & \text{otherwise}
%  \end{cases}
  \end{array}
\)

%\begin{eqnarray*}
%  x \ergte y & \ifandonlyif & y \erlte x \\
%  x \erlt y & \ifandonlyif & (x \erlte y) \land \lnot (x \ereq y) \\
%  x \ergt y & \ifandonlyif & (x \ergte y) \land \lnot (x \ereq y) \\
%  x \ersub y & = & x \erplus (\erneg y) \\
%  x \erdiv y & = & x \ermult \errecip{y} \\
%\end{eqnarray*}
\end{table}


\begin{table}[t]
\caption{
%   Axioms describing the behavior of $\erpinf$ and $\erninf$ with $w \in \affineExtend{S}$
   formalization of $\erpninf$'s and inverse of zero's behavior, $w \in \affineExtend{S}$}
\label{table:extended-real-inf}
%\(
% \begin{array}{lll@{\qquad\qquad}lll}
% \erpinf \erlte w & \ifandonlyif & w = \erpinf & w \erlte \erpinf \\
% w \erlte \erninf & \ifandonlyif & w = \erninf & \erninf \erlte w 
% \end{array}
%\)
%\medskip
\centering
\(
 \begin{array}{lll@{\qquad\qquad}lll}
 \erpinf \erlte w & \ifandonlyif & w = \erpinf & 
 w \erlte \erninf & \ifandonlyif & w = \erninf
 \end{array}
\)
\medskip

\(
  \begin{array}{l@{\qquad\qquad}lll@{\qquad\qquad}lll}
   w \erlte \erpinf &
   \erneg (\erpinf) & = & \erninf &
   \errecip{\erpinf} & = & 0
   \\
   \erninf \erlte w &
   \erneg (\erninf) & = & \erpinf &
   \errecip{\erninf} & = & 0 
  \end{array}
\)
\medskip

\(
  \begin{array}{lllll}
   w \erplus (\erpinf) & = & (\erpinf) \erplus w & = & 
   \begin{cases}
     \ernan & \text{if } w = \erninf \\
     \erpinf & \text{if } w \not= \erninf
   \end{cases} 
   \\
   w \erplus (\erninf) & = & (\erninf) \erplus w & = &
   \begin{cases}
     \ernan & \text{if } w= \erpinf \\
     \erninf & \text{if } w \not= \erninf
   \end{cases}
   \\
   w \ermult \erpinf & = & \erpinf \ermult w & = & 
   \begin{cases}
     \erpinf & \text{if } 0 \erlt w \\
     \erninf & \text{if } w \erlt 0 \\
     \ernan & \text{if } w = 0
   \end{cases}
   \\
   w \ermult \erninf & = & \erninf \ermult w & = &
   \begin{cases}
     \erninf & \text{if } 0 \erlt w \\
     \erpinf & \text{if } w \erlt 0 \\
     \ernan & \text{if } w = 0
   \end{cases}
   \\
   \errecip{0} & = & \erpinf
  \end{array}
\)
%\(
%  \begin{array}{lll@{\qquad\qquad}lll}
%   \erneg (\erpinf) & = & \erninf &
%   \errecip{\erpinf} & = & 0
%   \\
%   \erneg (\erninf) & = & \erpinf &
%   \errecip{\erninf} & = & 0 
%  \end{array}
%\)
\end{table}


%\begin{table}
%\centering
%\(
%  \errecip{0} = \erpinf
%\)
%\medskip
%  
%  \caption{Inverse of zero}
%  \label{table:extended-real-normal}
%\end{table}






\subsection{Rounding}

The second concept needed to formalize the \fpstd\ definition of operations
is that of rounding; a map that will take the intermediate result in
$\eR$ back into the set of floating-point numbers.  
We define it as a function that selects between the two adjoints of the
corresponding map into $\eR$.

More generally, let $(X,\sqsubseteq)$ be a partially ordered set 
that consists of one or more disjoint lattices, and 
let $\functiondef{v}{X}{\eR}$ be an order-embedding function from $X$ 
into the extended reals such that $\set{\erpinf, \erninf, \ernan} \subset v(X)$.
Then, the \emph{upper adjoint} and \emph{lower adjoint} of $v$ are respectively
the unique functions 
$\functiondef{\upperA{v}}{\eR}{X}$ and $\functiondef{\lowerA{v}}{\eR}{X}$
such that for all $r \in \eR$.
%\[
%  \begin{array}{lcrl}
%    \upperA{v}(r) & = & f & \text{where }
%      r \erlte v(f) \land
%         \forall g \in X . r \erlte v(g) \limplies f \sqsubseteq g \\
%    \lowerA{v}(r) & = & f & \text{where }
%      v(f) \erlte r \land
%         \forall g \in X . v(g) \erlte r \limplies g \sqsubseteq f
%  \end{array}
%\]
\begin{itemize}
\item
 $r \erlte v(\upperA{v}(r))$ and
 $\upperA{v}(r) \sqsubseteq x$ for all $x \in X$ with $r \erlte v(x)$;
\item
 $v(\lowerA{v}(r)) \erlte r$ and
 $x \sqsubseteq \lowerA{v}(r)$ for all $x \in X$ with $v(x) \erlte r$.
\end{itemize}
%
The function $\upperA{v}$ maps an element $r$ to the smallest element of $X$ 
that projects above $r$ (rounding up) 
while $\lowerA{v}$ maps $r$ to the largest element of $X$ 
that projects below $r$ (rounding down).
Let $\Bool = \{\top,\bot\}$ be the Booleans, with $\top$ being the true value.
We define a family of (higher-order) rounding functions: 
\[
  \functiondef{\round}{\RND \cross \Bool \cross \eR}
                      {\functionspace{(\functionspace{X}{\eR})}
                                     {(\functionspace{\eR}{X})}}
\]
parametrized by the partially ordered set $X$, 
which provides a systematic way of selecting between rounding up and rounding down.
Given a map $\functiondef{v}{X}{\eR}$, the rounding function returns one of $v$'s two adjoints, based on three previous inputs.
The first is the \emph{rounding mode}, chosen from the set:
\begin{equation*}
  \RND ~=~ \set{\rne, \rna, \rtp, \rtn, \rtz}
\end{equation*}
which represents the five rounding modes defined by \fpstd, namely, 
round to nearest with ties picking even value ($\rne$), 
round to nearest with ties away from zero ($\rna$), 
round towards $\erpinf$ ($\rtp$), 
round towards $\erninf$ ($\rtn$), and 
round towards zero ($\rtz$).
The second input of $\round$ is a Boolean value determining the sign of zero
when $X$ contains signed zeros (which is the case when $X$ is a set of
floating-point numbers).
The third input is the value to be rounded, needed because the 
rounding direction may depend on it (for example, when
rounding to the nearest element of $X$).  


\begin{table}
\caption{Definition of $\round$ }
\label{table:definition-of-round}
\centering
\(
\begin{array}{l}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   \round(\rne,s,r)(v) =
  \begin{cases}
\upperA{v} & r \not= 0,\, \lnot \lh{X}(r,v),\, \lnot \tb{X}(r,v)\\
\upperA{v} & r \not= 0,\, \tb{X}(r,v),\, \even{X}(\upperA{v}(r)) \\
\lowerA{v} & r \not= 0,\, \tb{X}(r,v),\, \even{X}(\lowerA{v}(r)) \\
\lowerA{v} & r \not= 0,\, \lh{X}(r,v)\\
\fun{rsz}(s)(v)  & r = 0\\
%%-\round(\rne,s,-r)(v) & r < 0 \\
%\lambda x. -h(v)(-x) & r < 0, h = \round(\rne,s,-r) \\
\lowerA{v} & r = \fnan
  \end{cases}
%S
%\intertext{\CTComment{This definition is incorrect. 
%The last case should be something like $\lambda y.\,\ernan$.
%Similarly, the previous one should be $\lambda y. {-}h(v)(y)$.
%The definition of $\round(\rna,s,r)(v)$ is similarly incorrect.
%I think it would actually be better to change the type of $\round$ and let it return 
%a value of $X$, not a function in $\functionspace{\eR}{X}$.
%}\TWComment{was my impression, too; see ``generality''}}
\\[1.2ex]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \round(\rna,s,r)(v) =
  \begin{cases}
\upperA{v} & r > 0,\, \lnot \lh{X}(r,v) \\
\lowerA{v} & r > 0,\, \lh{X}(r,v) \\
\fun{rsz}(s)(v)  & r = 0\\
%%-\round(\rna,s,-r)(v) & r < 0 \\
%\lambda x. -h(v)(-x) & r < 0,\, h = \round(\rna,s,-r) \\
\upperA{v} & r < 0,\, \lnot \lh{X}(r,v),\, \lnot \tb{X}(r,v) \\
\lowerA{v} & r < 0,\, \lh{X}(r,v) \lor \tb{X}(r,v) \\
\lowerA{v} & r = \fnan
  \end{cases}
\\[1.2ex]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \round(\rtp,s,r)(v) =
  \begin{cases}
    \fun{rsz}(s)(v)  & r = 0 \\
%    \lowerA{v} & r = 0, s = 0 \\
    \upperA{v} & \text{otherwise}
  \end{cases}
\\[1.2ex]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \round(\rtn,s,r)(v) =
  \begin{cases}
    \fun{rsz}(s)(v)  & r = 0 \\
%    \upperA{v} & r = 0, s = 1 \\
    \lowerA{v} & \text{otherwise}
  \end{cases}
\\[1.2ex]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \round(\rtz,s,r)(v) =
  \begin{cases}
    \lowerA{v} & r > 0 \\
    \fun{\fun{rsz}}(s)(v)  & r = 0 \\
%    \lowerA{v} & r = 0, s = 0 \\
    \upperA{v} & \text{otherwise}
  \end{cases} 
\end{array}
\)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
where
\[  \fun{rsz}(\top)(v) = \upperA{v} \qquad\qquad
  \fun{rsz}(\bot)(v) = \lowerA{v}
\]
\PRComment{I might not be up to date concerning the running discussion
on rounding, but how is the condition $r = \lowerA{v}$ supposed to
typecheck? lhs is a number, rhs is a function?}
\CTComment{That (i.e., $\fnan \quad r = \lowerA{v}$) must have been a typo. Fixed.
However, the last but one case of 
$\round(\rne,s,r)(v)$ and of
$\round(\rna,s,r)(v)$ are still wrong.}
\MBComment{I think these have been fixed.  Please check.}
\end{table}

The function $\round$ is defined in Table~\ref{table:definition-of-round}.
The definition relies on three auxiliary predicates $\lh{X}$, $\tb{X}$ and $\even{X}$
whose own definition depend on the particular domain $X$.
These express: when the value is in the \emph{lower half} of the interval
between two representations in $X$ (i.e. closer to $\lowerA{v}(r)$
than $\upperA{v}(r)$); the \emph{tie-break} condition when it is equal
distance from both; and whether a representation is even.
For illustration purposes, we provide a definition of those predicates
in Table~\ref{table:preds:Z} for when $X = \Z$, the set of integers with the usual ordering.
A definition of those predicates for sets of floating-point numbers is given later,
after we formalize such sets.
\MBComment{Generally, have I captured the overflow conditions correctly?}
\MBComment{Checking still appreciated!}


The fairly elaborate definition of $\round$ is motivated 
by our goal to provide an accurate model of rounding as defined in \fpstd.  
In particular, there is no mathematical reason for not using exclusively 
$\upperA{v}$ or $\lowerA{v}$ in it.
However, doing so would fail to reflect some properties 
of \fpstd\ floating-point numbers, for example 
``the sign of a sum [...] differs from at most one of the addends' signs''~\cite{IEEE754}.
For brevity, we will write $\fun{rnd}(v,m,s,r)$ for the application
$\round(m,s,r)(v)(r)$ which returns the value of $X$ that the real number $r$ 
is rounded to by using $v$.


\begin{table}[t]
\caption{Auxiliary predicates for round in the case $X = \closedAffineExtend{\Z}$}
\centering
\(
\begin{array}{rcl}
  \lh{\closedAffineExtend{\Z}}(r,v) & := & r - v(\lowerA{v}(r)) < v(\upperA{v}(r)) - r \\
  \tb{\closedAffineExtend{\Z}}(r,v) & := & r - v(\lowerA{v}(r)) = v(\upperA{v}(r)) - r  \\
  \even{\closedAffineExtend{\Z}}(x) & := & \exists z \in \Z \st x = 2 * z
\end{array}
\)
\label{table:preds:Z}
\end{table}
