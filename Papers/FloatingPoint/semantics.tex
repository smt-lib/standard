%!TEX root =  pap.tex

\section{Models of Floating-Point Arithmetic}
\label{section:models}

In this section we specify a set of (many-sorted) structures in the sense 
of model theory. These are the intended models of a logical theory of
floating-point numbers that reflects \fpstd.
In the next section we will specify a signature for such a theory and
show how each sort, function and predicate symbol in the signature is
interpreted over this set of structures.

\subsection{Universe}

The universe of each of our models consists of multiple sets:
one for the rounding modes and one for each of the different
floating-point precisions.  
Floating-point numbers other than $\fnan$ are triples of bit vectors
modelling the three components (sign, exponent and significand) of the
representations in \fpstd. 
We identify bit vectors of length $\nu > 0$ with elements of the function space
$\BV{\nu} = \functionspace{\N_\nu}{\set{0,1}}$
where $\N_\nu = \{0,\ldots, \nu-1\}$.
We write 
$\ones{\nu}$ for the unique function in $\functionspace{\N_\nu}{\set{1}}$ and
$\zeros{\nu}$ for the unique function in $\functionspace{\N_\nu}{\set{0}}$,
which represent respectively the bit vector of length $\nu$ containing 
all ones and that containing all zeros.
%
Let $\functiondef{\ubin{\nu}}{\BV{\nu}}{\N}$ and
$\functiondef{\sbin{\nu}}{\BV{\nu}}{\Z}$ denote the usual unsigned and 2's
complement encodings of bit vectors into integers.
Let  $\BVTriple{\mu}{\nu}$ denote the set $\BV{1} \cross \BV{\mu} \cross \BV{\nu-1}$.
For all integers $\ebits, \sbits > 1$, we define the set 
of floating-point numbers with $\ebits$ exponent bits and 
$\sbits$ significand bits\footnote{
Allowing arbitrary values for $\ebits$ and
$\sbits$ is strictly a generalization of \fpstd, which only defines a
handful of precisions.
However, doing so supports a wider range of
applications with little additional notation and effort.
} 
as the set $\F{\ebits}{\sbits} ~=~ \FBV{\ebits}{\sbits} \union \set{\fnan}$
%\begin{equation*}
%  \F{\ebits}{\sbits} ~=~ \FBV{\ebits}{\sbits} \union \set{\fnan}
%\end{equation*}
where

\vspace*{-1ex}
{\small
\begin{align*}
%  \F{\ebits}{\sbits} & ~=~  \FBV{\ebits}{\sbits} \union \set{\fnan} \\
  \FBV{\ebits}{\sbits} & ~=~ \FBVZ{\ebits}{\sbits} \union \FBVS{\ebits}{\sbits} \union
                             \FBVN{\ebits}{\sbits} \union \FBVI{\ebits}{\sbits} \\
  \FBVZ{\ebits}{\sbits} & ~=~ \setst{(s,e,m) \in \BVTriple{\ebits}{\sbits}}{ 
                                     e = \zeros{\ebits},\, m = \zeros{\sbits-1} } \\
  \FBVS{\ebits}{\sbits} & ~=~ \setst{(s,e,m) \in \BVTriple{\ebits}{\sbits}}{ 
                                     e = \zeros{\ebits},\, m \not= \zeros{\sbits-1} } \\
  \FBVN{\ebits}{\sbits} & ~=~ \setst{(s,e,m) \in \BVTriple{\ebits}{\sbits}}{ 
                              e \not= \ones{\ebits},\, e \not= \zeros{\ebits} } \\
  \FBVI{\ebits}{\sbits} & ~=~ \setst{(s,e,m) \in \BVTriple{\ebits}{\sbits}}{ 
                              e = \ones{\ebits},\, m = \zeros{\sbits-1} }
\end{align*}
}
The last four sets above correspond respectively to the bit vector triples used 
to represent zeros, subnormal numbers, normal numbers and infinities in \fpstd,
with the three components storing respectively sign, exponent and significand 
of the floating-point number.\footnote{
The significand component has length $\sigma-1$ because the hidden bit, 
which is 1 for normal numbers, is not explicitly represented.
}
We will write informally $-0$ and $+0$ to refer to the two elements 
of $\FBVZ{\ebits}{\sbits}$.

We fix a total order $\flte$ over $\FBV{\ebits}{\sbits}$ such that
$(s_1,e_1,m_1) \flte (s_2,e_2,m_2)$ if one of the following holds:

{\small
\begin{itemize}
\item $s_1 = 1, s_2 = 0$
\item $s_1 = 0, s_2 = 0,\, 
       \ubin{\ebits}(e_1) \lt \ubin{\ebits}(e_2)$
\item $s_1 = 0, s_2 = 0,\,
       \ubin{\ebits}(e_1) = \ubin{\ebits}(e_2),\,
       \ubin{\sbits}(m_1) \lte \ubin{\sbits}(m_2)$
\item $s_1 = 1, s_2 = 1,\,
       \ubin{\ebits}(e_2) \lt \ubin{\ebits}(e_1)$
\item $s_1 = 1, s_2 = 1,\,
       \ubin{\ebits}(e_1) = \ubin{\ebits}(e_2),\,
       \ubin{\sbits}(m_2) \lte \ubin{\sbits}(m_1)$
\end{itemize}
}
%
%\begin{equation*}
%%  (b_s,b_e,b_m) \flte (c_s,c_e,c_m)
%%   \ifandonlyif
%   \begin{array}{l}
%  (b_s = 1 \land c_s = 0) \lor\mbox{} \\
%  (b_s = 0 \land c_s = 0 \land \ubin{\ebits}{b_e} \lt \ubin{\ebits}{c_e}) \lor\mbox{} \\
%  (b_s = 0 \land c_s = 0 \land \ubin{\ebits}{b_e} = \ubin{\ebits}{c_e} \land
%        \ubin{\sbits}{b_m} \lte \ubin{\sbits}{c_m}) \lor\mbox{} \\
%  (b_s = 1 \land c_s = 1 \land \ubin{\ebits}{c_e} \lt \ubin{\ebits}{b_e}) \lor\mbox{} \\
%  (b_s = 1 \land c_s = 1 \land \ubin{\ebits}{b_e} = \ubin{\ebits}{c_e} \land
%      \ubin{\sbits}{c_m} \lte \ubin{\sbits}{b_m})     
%   \end{array}
%\end{equation*}

\noindent
We extend $\flte$ to a partial order on $\F{\ebits}{\sbits}$ 
by %defining 
$\fnan \flte \fnan$.

As discussed in Section \ref{section:formal-foundations}, 
we define operations over $\F{\ebits}{\sbits}$ analogously 
to those defined over $\eR$ 
by mapping floating-point values to extended reals,
performing the corresponding extended reals operation and then
rounding the result back to a floating-point value. 
To formalize this we define a function
$\functiondef{\fpv{\ebits,\sbits}}{\F{\ebits}{\sbits}}{\eR}$
which maps each floating-point number to the extended real it represents.
%\observation{ 
%  The function maps
%  $\FBVZ{\ebits}{\sbits} \union \FBVS{\ebits}{\sbits} \union \FBVN{\ebits}{\sbits}$ 
%  into the dyadic rationals.  Thus it is conceivable to use an
%  extended set of dyadic rationals rather than $\eR$.  That extended set would be
%  closed under addition, negation and multiplication but, critically,
%  not under reciprocal or square root.  Extending the rationals
%  give reciprocal, a real sub-field of the algebraic numbers would
%  give square root and extending the computable reals would give all
%  numbers that are likely to be meaningful for the application. $\eR$
%  is used for compatibility with the informal specification given in \fpstd.
%}  
% 
Let $\bias(\ebits) = 2^{\ebits - 1} - 1$.


\vspace*{-2ex}
{\small
\begin{align*}
  &\fpv{\ebits,\sbits}(f) = \fpv{\ebits,\sbits}((s,e,m)) =  \\
  &~~\begin{cases}
    0 & f \in \FBVZ{\ebits}{\sbits} \\
%    -1^{\ubin{1}{s}} \cdot 2^{\ubin{\ebits}{e} - \bias(\ebits)} \cdot (0 + \frac{\ubin{\sbits - 1}{\mu}}{2^{\sbits - 1}})  & (s,e,m)  \in \FBVS{\ebits}{\sbits} \\
    (-1)^{\ubin{1}(s)} \cdot 2^{1 - \bias(\ebits)} \cdot (0 + \frac{\ubin{\sbits - 1}(m)}{2^{\sbits - 1}})  & f \in \FBVS{\ebits}{\sbits} \\
    (-1)^{\ubin{1}(s)} \cdot 2^{\ubin{\ebits}(e) - \bias(\ebits)} \cdot (1 + \frac{\ubin{\sbits - 1}(m)}{2^{\sbits - 1}})  & f  \in \FBVN{\ebits}{\sbits} \\
    (-1)^{\ubin{1}(s)} \cdot (\erpinf) & f \in \FBVI{\ebits}{\sbits} \\
%    \ernan & f = \fnan
  \end{cases}
  \\[1ex]
  &\fpv{\ebits,\sbits}(\ernan) =  \fnan
\end{align*}
}
%
For brevity we will write just $\fpv{}$ in place of $\fpv{\ebits,\sbits}$
when the values $\ebits$ and $\sbits$ are clear from context or not important. 
One can show that $\fpv{}$ is injective over 
$\F{\ebits}{\sbits} \setminus \FBVZ{\ebits}{\sbits}$ and monotonic.
Thanks to the latter we have that
both $\upperA{\fpv{}}$ and $\lowerA{\fpv{}}$ are well defined\observation{
These are surjections for all points except
$\FBVZ{\ebits}{\sbits}$ which has the curious property that
$-0 = \upperA{\fpv{}}(0) \flte \lowerA{\fpv{}}(0) = +0$.
}
and so the function $\round$ can be used to map back 
from $\eR$ to $\F{\ebits}{\sbits}$.
The auxiliary predicates used in the definition of rounding 
in the case of $X = \F{\ebits}{\sbits}$ are defined in Table~\ref{table:preds:F}.
%
Both $\lh{\F{\ebits}{\sbits}}$ and $\tb{\F{\ebits}{\sbits}}$ use a set of
floating-point numbers with one extra significand bit.  This is
equivalent to the \emph{guard bit} used in hardware implementations,
giving a point mid-way between $\upperA{\fpv{}}$ and
$\lowerA{\fpv{}}$.  
The predicate $\tb{\F{\ebits}{\sbits}}$ captures the property of $r$ being
equidistant from $\lowerA{\fpv{}}(r)$ and
$\upperA{\fpv{}}(r)$, which means that any further significand bits would be
$0$.  This is equivalent to the \emph{sticky bit} used in hardware being equal to $0$.


\begin{table}[t]
\caption{Auxiliary predicates for round in the case $X = \F{\ebits}{\sbits}$}
\centering
\(
\begin{array}{rcl}
  \even{\F{\ebits}{\sbits}}(f) & := & f = (s,e,m) \in \FBV{\ebits}{\sbits} \,\land\,
  \even{\closedAffineExtend{\Z}}(\ubin{\sbits-1}(m)) 
  \\[1ex]
  \lh{\F{\ebits}{\sbits}}(r,\fpv{}) & := &
   \sbits' = \sbits + 1 \,\land\,
   \fpv{}(\lowerA{\fpv{}}(r)) = \fpv{\ebits,\sbits'}(\lowerA{\fpv{\ebits,\sbits'}}(r))
  \\[1ex]
  \tb{\F{\ebits}{\sbits}}(r,\fpv{}) & := &
   \sbits' = \sbits + 1 \,\land\,
   \fpv{}(\lowerA{\fpv{}}(r)) \erlt
   \fpv{\ebits,\sbits'}(\lowerA{\fpv{\ebits,\sbits'}}(r)) = {}
   \\[1ex]
   & &
   \fpv{\ebits,\sbits'}(\upperA{\fpv{\ebits,\sbits'}}(r)) \erlt
   \fpv{\ebits,\sbits'}(\upperA{\fpv{}}(r))
\end{array}
\)

\label{table:preds:F}
\end{table}

\subsection{Relations}
\label{section:relations}

Having defined a universe for the models, we next define various relations
which will be used as the interpretation of predicates in the theory
of floating-point.  Every relation is parameterized by a floating-point domain,
so each definition here actually describes a whole \emph{family} of relations.


\subsubsection{Unary Relations}

We consider the following unary relations (subsets) 
for classifying floating-point numbers as well as determining their sign, if
applicable%
\begin{longv}
(see Section \ref{section:limitations-and-omissions} for
further discussion of sign)
\end{longv}%
.\observation{
These definitions imply 
$f = \fnan \ifandonlyif \lnot
 (\fun{isNeg}_{\ebits, \sbits}(f)
  \lor \fun{isPos}_{\ebits, \sbits}(f))$.
}


\vspace*{-1ex}
{\small
\begin{align*}
%  \fun{isNormal}_{\ebits, \sbits} & = & \FBVN{\ebits}{\sbits} \\
%  \fun{isSubnormal}_{\ebits, \sbits} & = & \FBVS{\ebits}{\sbits} \\
%  \fun{isZero}_{\ebits, \sbits} & = & \FBVZ{\ebits}{\sbits} \\
%  \fun{isInfinite}_{\ebits, \sbits} & = & \FBVI{\ebits}{\sbits} \\
%  \fun{isNaN}_{\ebits, \sbits} & = & \set{\fnan} \\
  \fun{isNeg}_{\ebits, \sbits} & ~=~ \setst{f \in \FBV{\ebits}{\sbits}}{f = (1,e,m)} \\
  \fun{isPos}_{\ebits, \sbits} & ~=~ \setst{f \in \FBV{\ebits}{\sbits}}{f = (0,e,m)} 
\end{align*}
}
\vspace*{-2ex}

\subsubsection{Binary Relations}
\label{section:binary-relations}

We define a number of binary relations for comparing floating-point
numbers.  These are different from the equality and ordering relations
on $\F{\ebits}{\sbits}$ (i.e., $=$ and $\flte$) and those on $\eR$ (i.e., $=$
and $\erlte$).
Despite their names, they are not actually equality or
ordering relations as they do not contain $(\fnan,\fnan)$ and
$\fun{eq}$, $\fun{leq}$ and $\fun{geq}$ contain both $(+0,-0)$ and $(-0,+0)$.

\vspace*{-2ex}
{\small
\begin{align*}
  \fun{eq}_{\ebits, \sbits} & ~=~
  \setst{(f,g) \in \FBV{\ebits}{\sbits} \cross \FBV{\ebits}{\sbits}}
        {\fpv{}(f) = \fpv{}(g)} \\
  \fun{leq}_{\ebits, \sbits} & ~=~
  \setst{(f,g) \in \FBV{\ebits}{\sbits} \cross \FBV{\ebits}{\sbits}}
        {\fpv{}(f) \erlte \fpv{}(g)} \\
  \fun{lt}_{\ebits, \sbits} & ~=~
  \setst{(f,g) \in \FBV{\ebits}{\sbits} \cross \FBV{\ebits}{\sbits}}
        {\fpv{}(f) \erlt \fpv{}(g)} \\
  \fun{geq}_{\ebits, \sbits} & ~=~
  \setst{(f,g) \in \FBV{\ebits}{\sbits} \cross \FBV{\ebits}{\sbits}}
        {\fpv{}(f) \ergte \fpv{}(g)} \\
  \fun{gt}_{\ebits, \sbits} & ~=~
  \setst{(f,g) \in \FBV{\ebits}{\sbits} \cross \FBV{\ebits}{\sbits}}
        {\fpv{}(f) \ergt \fpv{}(g)}
\end{align*}
}


\subsection{Operations}
\label{section:operations}

Similarly to relations, we define families (parameterized by domains)
of functions which will serve as the interpretation of various
operations in the theory of floating-point.

\paragraph{Sign Operations}
Two operations, negation and absolute value, manipulate the sign of the number.  
Since the domains of floating-point numbers are symmetric around $0$, there is no
need for rounding and the operations can be defined directly on the
floating-point bit vectors without using $\eR$.  

\vspace*{-2ex}
{\small
\begin{align*}
 & \functiondef{\fun{neg}_{\ebits, \sbits}}{\F{\ebits}{\sbits}}{\F{\ebits}{\sbits}} \\
 & \fun{neg}_{\ebits, \sbits}(f)  ~=~
  \begin{cases}
    (\lnot s,e,m) & f = (s,e,m) \in \FBV{\ebits}{\sbits} \\
    \fnan & f = \fnan
  \end{cases} 
  \\[1.2ex]
 & \functiondef{\fun{abs}_{\ebits, \sbits}}{\F{\ebits}{\sbits}}{\F{\ebits}{\sbits}} 
\\
 & \fun{abs}_{\ebits, \sbits}(f) ~=~
  \begin{cases}
    (0,e,m) & f = (s,e,m) \in \FBV{\ebits}{\sbits} \\
    \fnan & f = \fnan
  \end{cases}
\end{align*}
}


\paragraph{Arithmetic Operations}

The main operations on floating-point numbers are those that
correspond to the operations on an ordered field.  
They are defined by mapping arguments to $\eR$ with $\fpv{}$,
performing the corresponding operation in $\eR$, and finally 
mapping the result back with $\round$.
%


\vspace*{-2ex}
{\small
\begin{align*}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  &\functiondef{\fun{add}_{\ebits, \sbits}}{\RND \cross \F{\ebits}{\sbits} \cross \F{\ebits}{\sbits}}{\F{\ebits}{\sbits}} \\
  &\fun{add}_{\ebits, \sbits}(\rma,f,g) ~=~ \fun{rnd}(\fpv{}, \rma, \fun{addSign}(\rma,f,g),
    \fpv{}(f) + \fpv{}(g) )
%
\\[1.2ex]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  & \functiondef{\fun{sub}_{\ebits, \sbits}}{\RND \cross \F{\ebits}{\sbits} \cross \F{\ebits}{\sbits}}{\F{\ebits}{\sbits}} \\
  & \fun{sub}_{\ebits, \sbits}(\rma,f,g) ~=~
  %&\quad
  \begin{array}[t]{@{}l@{}l@{}}
    \fun{rnd}(&\fpv{}, \rma, \fun{subSign}(\rma,f,g),
    \fpv{}(f) - \fpv{}(g) ) 
  \end{array}
\\[1.2ex]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  & \functiondef{\fun{mul}_{\ebits, \sbits}}{\RND \cross \F{\ebits}{\sbits} \cross  \F{\ebits}{\sbits}}{\F{\ebits}{\sbits}} \\
  & \fun{mul}_{\ebits, \sbits}(\rma,f,g) ~=~ %\\
  %&\quad
  \begin{array}[t]{@{}l@{}l@{}}
    \fun{rnd}(&\fpv{}, \rma, \fun{xorSign}(f,g),
\fpv{}(f) * \fpv{}(g) )
  \end{array}
%
\\[1.2ex]
  &\functiondef{\fun{div}_{\ebits, \sbits}}{\RND \cross \F{\ebits}{\sbits} \cross  \F{\ebits}{\sbits}}{\F{\ebits}{\sbits}} \\
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
  &\fun{div}_{\epsilon,\sigma}(\rma, f, g) ~=~ \\
  &\quad 
  \begin{cases}
   \fun{neg}_{\ebits, \sbits}(\fun{rnd}(v,\rma, \top, -(\fpv{}(f)/\fpv{}(g))))   & \fun{xorSign}(f,g) \\
                          \fun{rnd}(v,\rma, \bot,  \fpv{}(f)/\fpv{}(g))    & \lnot \fun{xorSign}(f,g)
  \end{cases}
%
%  &\fun{div}_{\ebits, \sbits}(\rma,f,g) ~=~ %\\
%  %&\quad
%  \begin{array}[t]{@{}l@{}l@{}}
%    \fun{rnd}(&\fpv{}, \rma, \fun{xorSign}(f,g),
%    \fpv{}(f) / \fpv{}(g) )
%  \end{array}
%
\\[1.2ex]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
 & \functiondef{\fun{fma}_{\ebits, \sbits}}{\RND \cross \F{\ebits}{\sbits} \cross \F{\ebits}{\sbits} \cross \F{\ebits}{\sbits}}{\F{\ebits}{\sbits}} \\
 & \fun{fma}_{\ebits, \sbits}(\rma,f,g,h) ~=~ \\
 & \quad
  \begin{array}[t]{@{}l@{}l@{}}
    \fun{rnd}(&\fpv{}, \rma, \fun{fmaSign}(\rma,f,g,h),
     (\fpv{}(f) * \fpv{}(g)) + \fpv{}(h) )
  \end{array}
\end{align*}
}

The $\fun{addSign}$, $\fun{subSign}$, $\fun{xorSign}$ and $\fun{fmaSign}$ 
predicates above are defined as follows ($\lxor$ denotes exclusive or):


{\small
\begin{align*}
  &\fun{addSign}(\rma,f,g) ~:=~
  \begin{cases}
    \fun{isNeg}(f) \land \fun{isNeg}(g) & \rma \not= \rtn \\
    \fun{isNeg}(f) \lor \fun{isNeg}(g) & \rma = \rtn \\
  \end{cases}
\end{align*}

\begin{align*}
  &\fun{xorSign}(f,g) ~:=~
   \fun{isNeg}(f) \lxor \fun{isNeg}(g)
\\[1.2ex]
  & \fun{fmaSign}(\rma,f,g,h) ~:=~ %\\
  %& \quad
    \fun{addSign}(\rma,\fun{mul}_{\ebits, \sbits}(\rma,f,g), h)
\\[1.2ex]
  & \fun{subSign}(\rma,f,g) ~:=~ %\\
  %& \quad
     \fun{addSign}(\rma, f, \fun{neg}_{\ebits, \sbits}(g))
\end{align*}
}
%
Note that since $\fun{fma}_{\ebits, \sbits}$ only calls $\round$ once,
it is not the same as $\fun{add}_{\ebits, \sbits}(\rma,\fun{mul}_{\ebits, \sbits}(\rma,a,b),c)$.
%
Also, $\fun{div}_{\epsilon,\sigma}$ is equal to 
$\fun{rnd}(\fpv{}, \rma, \fun{xorSign}(f,g), \fpv{}(f) / \fpv{}(g) )$
at all points except positive numbers divided by $-0$, where the
``obvious'' definition gives positive infinity while the definition
given above gives the correct result of minus infinity.

\paragraph{Additional operations}

\begin{longv}
\fpstd\ defines a square root function which returns the
floating-point number nearest to the square root of the real
represented by the input number.  Also $\fun{sqrt}(\rma,-0) =
-0$ since $-0$ represents $0 \in \eR$, whose square root is $0 \in \eR$ and
the sign is inherited when rounding is performed.
%


\vspace*{-2ex}
{\small
\begin{align*}
    & \functiondef{\fun{sqrt}_{\ebits, \sbits}}{\RND \cross \F{\ebits}{\sbits}}{\F{\ebits}{\sbits}}
    \\
%\end{align*}
%\begin{align*}
& \fun{sqrt}_{\ebits, \sbits}(\rma,f) = % \\
%&\quad
\begin{cases}
  \fun{rnd}(\fpv{}, \rma, \fun{isNeg}(f), z) & 0 \erlte x = \fpv{}(f), \\
        & z \ermult z =  x,\: 0 \erlte z
 \\
  \fnan & \text{otherwise}
\end{cases}
\end{align*}
}
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
While this is the natural definition of square root, it results in a few unexpected
consequences.  For example
$\fun{mul}(\rma,\fun{sqrt}(\rma_1,x),\fun{sqrt}(\rma_2,x))$ is not guaranteed
to be equal to $x$ and, depending on the three rounding modes 
%$\rma,\rma_1,\rma_2$,
may give an infinity, even when $x$ is a normal number.\observation{
$x \in \eR$ and thus $z$ is uniquely defined.
Given $f \in \F{\ebits}{\sbits}$ there is not necessarily a $g \in
\F{\ebits}{\sbits}$ such that $f = \fun{mul}_{\ebits, \sbits}(\rma,g,g)$ and
$\fun{leq}_{\ebits, \sbits}(0,g)$.  Furthermore, in the case of
subnormal numbers, there are potentially multiple, distinct, positive
square roots.  Thus care must be taken with implicit definitions of
$\fun{sqrt}$ just using $\fun{mul}$ or $\fun{fma}$.
}
\end{longv}

Let $\mathrm{w}_{\ebits,\sbits}$ be the restriction of $\fpv{\ebits, \sbits}$
to
\(
 \FBVI{\ebits}{\sbits} \union \set{\fnan} \union 
 \setst{f \in \F{\ebits}{\sbits}}{\fpv{\ebits, \sbits}(f) \in \Z}.
\)
The following operation rounds back to a subset of $\F{\ebits}{\sbits}$,
effectively rounding the value to a
whole number representable in the given floating-point format:
%


\vspace*{-2ex}
{\small
\begin{align*}
& \functiondef{\fun{rti}_{\ebits, \sbits}}{\RND \cross \F{\ebits}{\sbits}}{\F{\ebits}{\sbits}} \\
& \fun{rti}_{\ebits, \sbits}(\rma,f) = \fun{rnd}(\mathrm{w}_{\ebits,\sbits},\rma,\fun{isNeg}(f),\fpv{}(r))
\end{align*}
}
\PRComment{sounds right intuitively, but I have trouble
seeing through the details of using rnd with a partial function
$\mathrm{w}_{\ebits,\sbits}$}
\MBComment{$\mathrm{w}_{\ebits,\sbits}$ should not be partial; just
  have a restricted domain.}
\TWComment{I agree this is difficult to
  understand. If $\mathrm{w}_{\ebits,\sbits}$ is really just a
  restricted-domain function (i.e.\ it acts on integral FP numbers
  semantically like the identity), then where is the ``truncation''
  happening??}
\MBComment{It is happening in the rounding.  The use of $v$ projects
  any floating-point number into $\eR$, but we only want to round back
into $\mathrm{w}_{\ebits,\sbits}$ which round will do.  This is the
same trick we use for casting and for rounding to integer.}
%
Let $\functiondef{\fun{in}}{\eZ}{\eR}$ with $\fun{in}(z) = z$.\footnote{
We consider $\Z$ a subset of $\R$ and hence $\eZ$ a subset of $\eR$.
}
Similarly to $\fun{rti}_{\ebits, \sbits}$, the remainder operation requires rounding an intermediate value to an integer:
%

\vspace*{-2ex}
{\small
\begin{align*}
  & \functiondef{\fun{rem}_{\ebits, \sbits}}{\RND \cross \F{\ebits}{\sbits} \cross \F{\ebits}{\sbits}}{\F{\ebits}{\sbits}} \\
 & \fun{rem}_{\ebits, \sbits}(\rma,f,g) = \\
&\quad 
 \begin{cases}
  f & f \not\in \FBVI{\ebits}{\sbits} \union \set{\fnan}, g \in \FBVI{\ebits}{\sbits} \\
  \fnan & f \in \FBVI{\ebits}{\sbits} \union \set{\fnan}\\ 
  \fnan & g \in \FBVZ{\ebits}{\sbits} \union \set{\fnan} \\
  \fun{rnd}(\fpv{}, \rma, \fun{isNeg}(f), x) 
      & %b = \fun{isNeg}(f),\,
        x = \fpv{}(f) - (\fpv{}(g) * y ),\, \\
      & y = \fun{rnd}(\fun{in}, \rma, \bot, \fpv{}(f) / \fpv{}(g))
\end{cases} \\
  & \functiondef{\fun{remrne}_{\ebits, \sbits}}{\F{\ebits}{\sbits} \cross \F{\ebits}{\sbits}}{\F{\ebits}{\sbits}} \\
 & \fun{remrne}_{\ebits, \sbits}(f,g) = \fun{rem}_{\ebits, \sbits}(\rne,f,g)
\end{align*}
}
\MBComment{Please check the behavior of this function, esp. the
  negative values.}
Note that the remainder computed as above is always exact when $\rne$
is used.  This remainder function is the one used by
the C standard library but is not necessarily the same as the
intuitive idea of remainder which can be computed via:
$\fun{fma}_{\ebits, \sbits}(\rma,\fun{neg}_{\ebits, \sbits}(\fun{div}_{\ebits, \sbits}(\rma,f,g)),g,f)$.
%
The next two operations, the maximum and minimum of two floating-point numbers, 
are specified only partially: when the two arguments
have the same value in $\eR$, either one of the arguments can be returned.  
%


\vspace*{-1ex}
{\small
\begin{align*}
  & \functiondef{\fun{max}_{\ebits, \sbits}}{\F{\ebits}{\sbits} \cross \F{\ebits}{\sbits}}{\F{\ebits}{\sbits}} \\
& \fun{max}_{\ebits, \sbits}(f,g) = 
\begin{cases}
      f & \fun{gt}_{\ebits, \sbits}(f,g) \text{ or } g = \fnan \\
      g & \fun{gt}_{\ebits, \sbits}(g,f) \text{ or } f = \fnan \\
      h & h \in \{f,g\},\, \fun{eq}_{\ebits, \sbits}(f,g) 
\end{cases}
\\[1.2ex]
  & \functiondef{\fun{min}_{\ebits, \sbits}}{\F{\ebits}{\sbits} \cross \F{\ebits}{\sbits}}{\F{\ebits}{\sbits}} \\
& \fun{min}_{\ebits, \sbits}(f,g) =
\begin{cases}
      f & \fun{lt}_{\ebits, \sbits}(f,g) \text{ or } g = \fnan \\
      g & \fun{lt}_{\ebits, \sbits}(g,f) \text{ or } f = \fnan \\
      h & h \in \{f,g\},\, \fun{eq}_{\ebits, \sbits}(f,g) 
\end{cases}
\end{align*}
}
%
%\begin{shortv}
%{\small
%\begin{align*}
%  & \functiondef{\fun{max}_{\ebits, \sbits}}{\F{\ebits}{\sbits} \cross \F{\ebits}{\sbits}}{\F{\ebits}{\sbits}} \\
%& \fun{max}_{\ebits, \sbits}(f,g) = 
%\begin{cases}
%      f & \fun{gt}_{\ebits, \sbits}(f,g) \text{ or } g = \fnan \\
%      g & \fun{gt}_{\ebits, \sbits}(g,f) \text{ or } f = \fnan \\
%      h & h \in \{f,g\},\, \fun{eq}_{\ebits, \sbits}(f,g) 
%\end{cases}
%\end{align*}%
%}
%
%The minimum function $\fun{min}_{\ebits, \sbits}$ is defined similarly.
%\end{shortv}

Note that the underspecification is an issue only when one of the inputs to
$\fun{max}_{\ebits, \sbits}$ or $\fun{min}_{\ebits, \sbits}$ 
is $-0$ and the other is $+0$.
However, it means that we consider as acceptable models any structures 
with function families $\fun{max}_{\ebits, \sbits}$ and $\fun{min}_{\ebits, \sbits}$
that satisfy the specifications above, regardless of whether they return
$-0$ or $+0$ for $(-0,+0)$, and for $(+0,-0)$.
This is necessary because \fpstd\ itself allows either value to be returned,
and compliant
implementations do vary.  For example, on some Intel processors the
result returned by the x87 and SSE units is different.

%\observation{Using one of the order comparisons in Section
%  \ref{section:binary-relations} to select one or the other argument
%  gives an implementations of maximum or minimum.  However these are
%  deterministic and thus will fix one result for the cases of $+0/-0$
%  and $+0/-0$.}


All the preceding operations have floating-point input and outputs
in the same set, $\F{\ebits}{\sbits}$.
To convert between different floating-point domains the following map
is needed:


\vspace*{-2ex}
{\small
\begin{align*}
  & \functiondef{\fun{cast}_{\ebits,\sbits,\ebits',\sbits'}}{\RND \cross \F{\ebits'}{\sbits'}}{\F{\ebits}{\sbits}} \\
 & \fun{cast}_{\ebits',\sbits',\ebits,\sbits}(\rma,f) ~=~ 
  \fun{rnd}(\fpv{\ebits,\sbits}, \rma,\fun{isNeg}(f),\fpv{\ebits',\sbits'}(f))
\end{align*}
}
%
If $\ebits \gte \ebits'$ and $\sbits \gte \sbits'$, the rounding mode argument
is irrelevant since then all values of $\F{\ebits'}{\sbits'}$ are 
representable exactly in $\F{\ebits}{\sbits}$.  
However, this cannot be regarded as syntactic sugar 
because whether a value is a normal or a subnormal number does change 
depending on the floating-point domain.

\subsection{Combinations with Other Theories}
\label{section:conversions}

For many applications, the theory of floating-point is not sufficient
to reason about the full problem; other theories such as integers, bit vectors,
or reals are needed as well.
This section describes the extensions to the intended models required to account 
for these additional domains, and possible mappings between them.
%
\fpstd\ includes a number of functions to convert to ``integer formats.''
We define here such conversions
as well as extensions to \fpstd\ covering conversion 
to real and from real and bit vectors.
%
Many of the additional operations are underspecified in that 
\emph{out of bounds} and other \emph{error} conditions 
do not have prescribed return values.

\subsubsection{Real Numbers}
For a model of the theory of floating-point to also be a model of the
theory of reals, its universe has to be extended to a
disjoint union with $\R$.  
Using the connections between $\F{\ebits}{\sbits}$ and $\eR$, 
we add the following two conversion operations:
%

\vspace*{-2ex}
{\small
\begin{align*}
 & \functiondef{\fun{realToFP}_{\ebits, \sbits}}{\RND \cross \R}{\F{\ebits}{\sbits}} \\
 & \fun{realToFP}_{\ebits, \sbits}(\rma,r) ~=~ \fun{rnd}(\fpv{}, \rma, \bot,r)
\\[1.2ex]
 & \functiondef{\fun{FPToReal}_{\ebits, \sbits}}{\F{\ebits}{\sbits}}{\R} \\
 & \fun{FPToReal}_{\ebits, \sbits}(f) ~=~
   \begin{cases}
    \fpv{}(f) & \fpv{}(f) \in \R \\
    x & x \in \R \text{, otherwise}
   \end{cases}
\end{align*}
}
%
\TWComment{so we dictatorially decide that real 0 is $+0$ in FP? Since we
  have allowed a lot of nondeterminism in other conversion situations,
  this seems unmotivated}
\MBComment{I see no reason why we can't change this, although I'm not
  entirely sure what notation to use.  Cesare?}
\MBComment{Ah!  We make the same choice for the conversions from
  integers to floating-point, which I believe to be well motivated
  from hardware designs and would be a pain to have to assert if we
  made it non-deterministic.  Thus an argument can be made for consistency.}
%
We do not specify what the value of $\fun{FPToReal}_{\ebits, \sbits}(f)$ is when $f$ does not correspond to a real number.
This means again that we accept as a model any structure with a function family
$\fun{FPToReal}_{\ebits, \sbits}$ that satisfies the specification above.


\subsubsection{Fixed-size Bit Vectors}

Similarly to the previous case, to form a joint model of the theories of floating-point and
fixed-width bit vectors, the domain must be extended to a disjoint
union with $\BV{\nu}$ for every $\nu > 0$.  
Let $\functiondef{\conc}{\BV{\mu} \cross \BV{\nu}}{\BV{\mu+\nu}}$ be 
the bit vector concatenation function for each $\mu,\nu > 0$.
The following function converts a bit vector of length $\ebits+\sbits$, 
with $\ebits,\sbits > 1$,
to a floating-point number in $\F{\ebits}{\sbits}$ by slicing the bit vector in three:
%

\vspace*{-1ex}
{\small
\begin{align*}
 & \functiondef{\fun{bitpatternToFP}_{\ebits, \sbits}}{\BV{\ebits + \sbits}}{\F{\ebits}{\sbits}} 
 \\
%\begin{align*}
 & \fun{bitpatternToFP}_{\ebits, \sbits}(b) ~=~ %\\
 %& \quad
   \begin{cases}
    (s,e,m) & b = s \conc e \conc m,\; \\
            & (s,e,m) \in \FBV{\ebits}{\sbits} \\
    \fnan & \text{otherwise}
   \end{cases}
\end{align*}
}

The function $\fun{bitpatternToFP}$ is not injective as there are
multiple bit-patterns which represent $\fnan$.  
This implies that it is not possible to give a reverse map without fixing the encoding 
of $\fnan$ to a particular value.  

The next two functions first convert the bit vector to the integer value 
it denotes in binary, in 2's complement and unsigned format respectively, and 
then round that value to the corresponding floating-point.
The last two functions do the inverse conversion.

\vspace*{-1ex}
{\small
\begin{align*}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  & \functiondef{\fun{sIntToFP}_{\nu,\ebits,\sbits}}{\RND \cross \BV{\nu}}{\F{\ebits}{\sbits}} \\
 & \fun{sIntToFP}_{\nu,\ebits,\sbits}(\rma,b) ~=~
   \fun{rnd}(\fpv{}, \rma, \bot, \sbin{\nu}(b))
\\[1.2ex]
%\intertext{\MBComment{Similar note about this not generating $-0$}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  & \functiondef{\fun{uIntToFP}_{\nu,\ebits,\sbits}}{\RND \cross \BV{\nu}}{\F{\ebits}{\sbits}} \\
 & \fun{uIntToFP}_{\nu,\ebits,\sbits}(\rma,b) ~=~
   \fun{rnd}(\fpv{}, \rma, \bot, \ubin{\nu}(b))
\\[1.2ex]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  & \functiondef{\fun{FPToSInt}_{\nu,\ebits,\sbits}}{\RND \cross \F{\ebits}{\sbits}}{\BV{\nu}} \\
 & \fun{FPToSInt}_{\nu,\ebits,\sbits}(\rma,f) ~=~ %\\
 %& \quad 
   \begin{cases}
    b & \sbin{\nu}(b) = \fun{rnd}(\fun{in}, \rma, \bot, \fpv{}(f)) \\
    \fnan & \text{otherwise}
   \end{cases}
\\[1.2ex]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  & \functiondef{\fun{FPToUInt}_{\nu,\ebits,\sbits}}{\RND \cross \F{\ebits}{\sbits}}{\BV{\nu}} \\
 & \fun{FPToUInt}_{\nu,\ebits,\sbits}(\rma,f) %~=~ \\
 %& \quad
   \begin{cases}
    b & \ubin{\nu}(b) = \fun{rnd}(\fun{in}, \rma, \bot, \fpv{}(f)) \\
    \fnan & \text{otherwise}
   \end{cases}
\end{align*}
}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "semantics"
%%% End: 

