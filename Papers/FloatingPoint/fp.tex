%!TEX root =  pap.tex

\section{Floating-Point Arithmetic}
\label{section:fp}

%Introduction to floating-point

\emph{Floating-point} refers to a way of encoding subsets of
the rational numbers using bit vectors of fixed width.
%
A floating point number consists of three such bit vectors: one for the
fractional part of the number, called the \emph{significand}, one for the
\emph{exponent} of an integer power by which the fractional part is
multiplied, and a single bit for the \emph{sign}.
For example:
%% \begin{eqnarray*}
%%   7.28125 & = & 7 + \frac{28125}{(2 * 5)^5}
%% %          & = & (2^{2} + 2^{1} + 2^{0}) + (2^{-2} + 2^{-5}) \\
%%            =  (2^{2} + 2^{1} + 2^{0}) + (2^{-2} + 2^{-5}) \\
%%           & = & 1.1101001_{2} * 2^{2}
%% \end{eqnarray*}
%

%%% needs a new paragraph to prevent incorrect line spacing
%\vspace*{-1ex}
{
\begin{shortv}
\small
\end{shortv}
\[
%\begin{eqnarray*}
\begin{array}{lll}
  7.28125 & = & 7 + 28125 \cdot 10^{-5} %\\
          \ = \ (2^{2} + 2^{1} + 2^{0}) + (2^{-2} + 2^{-5}) \\
          & = & 111.01001_2 \ = \ 1.1101001_2 \cdot 2^{2}
\end{array}
%\end{eqnarray*}
\]
}

\noindent
can be represented as a \emph{binary} floating point number with a sign bit of \texttt 0, an
exponent of \texttt{2} and a significand of \texttt{1101001}: the leading $1$ before
the binary point is omitted (for so-called \emph{normal} numbers) and hence
known as the \emph{hidden bit}.
%% \observation{Binary
%%   floating-point generally does not store the leading $1$ of the significand if the
%%   number is in normal form. This bit is referred to as the 'hidden bit'.}

Arithmetic on floating point numbers is
defined as performing the operation \emph{as if the numbers were reals},
followed by rounding the result to the nearest value representable as a
floating point number; ``nearest'' is defined by a set of rules called a
\emph{rounding mode}.
%
Many floating-point systems implemented in computer processors
have included special values such as infinities
and not-a-number ($\fnan$). When an underflow, overflow or other
exceptional condition occurs, these special values can be returned instead of 
triggering an interrupt.  This can simplify the control circuitry and
results in faster computation but at the cost of making the floating-point
number system more complex. \fpstd\ standardizes, in different
floating-point formats, the sizes of the bit vectors used for number
presentation, as well as the various rounding modes, and the meaning and
use of special values.

%Historically there were a number of different floating-point systems
%in common usage.  This created significant problems when moving data
%between machines and made writing portable numerical software
%prohibitively difficult.  A standardisation process lead to $\ieeefp$,
%which defines multiple floating point systems including their
%semantics and representation as bit strings.  Since their introduction in
%1985, the $\ieeefp$ formats have become the dominant floating-point
%system and the most common way of representing non-integer quantities.
%Most systems that do not comply with $\ieeefp$, such as some GPUs,
%simply implement a subset of its features.  We focus on
%binary encodings of $\ieeefp$ 
%for which the definitive reference is the standard~\cite{IEEE754}.


%Floating-point reasoning is hard because
%1. combinatorics of number types
%2. rounding breaks associativity and distributivity

\begin{longv}
Formal reasoning about algorithms and programs that use floating-point numbers has acquired
a reputation of being difficult, complex and error prone~\cite{DBLP:journals/toplas/Monniaux08}. 
There are at least two causes for this. One is the combinatorial explosion in operations
due to the existence of special computational values. More precisely,
\fpstd\ defines five classes of numbers: normal, subnormal (which cannot
be expressed in normal form, due to limits on the exponent range),
zeros (a positive and a negative one, 
so that approximation from above and below can be
distinguished), infinities (one positive and one negative), and $\fnan$.
The behavior of each operation depends on the classes of its arguments,
potentially requiring a five-way case distinction for every operation
considered.
The other cause for the intricacies is rounding. 
Rounding is vital as it permits a fixed bound on
the amount of memory required to store and compute with floating-point
numbers. 
However, it also causes floating-point arithmetic to defy some fundamental laws
of traditional arithmetics,
% 
such as the associativity of addition\footnote{
Since rounding is performed after every operation, the expressions $(a+b)+c$ and
$a+(b+c)$ can evaluate to different values. 
}
As a result, many algebraic and
automated reasoning techniques 
based on these laws are inapplicable to it.
While challenging, reasoning formally and possibly automatically 
about floating-point numbers is crucial for many purposes.
\begin{itemize}
%  \item[NaN generation]{}
%  \item[Inf generation]{}
%  \item[Subnormal Generation]{}
  \item[{\bf Identifying the Generation of Special-Values}]
%
    It is generally useful to know whether a program may generate the
    special floating-point values infinity and $\fnan$. While their
    presence is not necessarily a bug,
    %ct omitted
%    \observation{\TWComment{Perhaps this
%        footnote could be omitted?}Special values are not necessarily a
%      bug. In some cases, identifying inputs that will generate them is as
%      hard as performing the computation. For example, when summing an
%      array of values, determining if an intermediate value will overflow.
%      In these cases, the intended programming model is to perform the
%      computation, check if it returns a special value and if so switch to
%      an alternative algorithm.} 
      they fall outside classical real-number
    arithmetic. In some cases, the detection of subnormal numbers is
    valuable as they have limited precision and can thus amplify errors.

%  \item[Catastrophic Cancellation]{}
%  \item[Loss of Significance]{}
  \item[{\bf Detecting Numerical Instabilities}]
%
    As a consequence of rounding, there are non-zero, normal numbers $x$
    and $y$ such that $x + y = x$. Although again this is not necessarily a bug, it is
    counter-intuitive for many programmers and may be a symptom of deeper
    algorithmic problems. Similarly, \emph{catastrophic cancellation}
    occurs in $x+y$ when the exponents of $x$ and $y$ are (near-)equal and
    their signs differ. In this case the most significant bits can cancel,
    effectively amplifying the errors introduced by rounding.

%  \item[Bounds on Conversion]{}
  \item[{\bf Exposing Undefined Behavior}]
%
    With few exceptions, \fpstd\ fully defines the results of all operations 
    on floating-point numbers but does not cover
    corner cases of conversions to other formats.  Although this may
    seem like a trivial issue, it was the immediate cause of the loss
    of Ariane 5 Flight 501.

  \item[{\bf Generating Test-Cases}]
%
    Many safety standards for embedded software mandate testing coverage
    criteria. Automatic test case generation is vital as system complexity
    rises, and is particularly important for showing which paths are
    infeasible, as developers often miss floating-point related
    corner cases. These applications demand model generating and
    bit-precise reasoning engines.

%  \item[Bounds]{}
%  \item[Error verses real]{}
  \item[{\bf Proving Functional Correctness}]
%
    Formal correctness specifications for numerical programs
    often include bounds on the output. 
%    The speed of light ($1.1168139 \cdot
%    2^{28}$) and the Planck length ($1.342686 \cdot 2^{-116}$), as well as many
%    other physical limits are expressible even in the range of
%    single-precision (32-bit) floating-point numbers. Thus most
%% systems
%    applications have `common sense' limits on their inputs and outputs,
%    checking which is a suitable task for a reasoning tool.
     Checking those bounds is a suitable task for a reasoning tool.
    Another common correctness measure is the difference between the result of the
    computation performed in floating-point and its real-arithmetic result.
    Care must be taken because most non-trivial numerical algorithms are not
    simple transcriptions of mathematical expressions into software;
    comparing the same algorithm over the two domains may be misleading.
\end{itemize}
%%
All these scenarios have in common that classical mathematical reasoning,
regardless of whether it is manual or automated,
is ineffective when dealing with floating-point.
\end{longv}
