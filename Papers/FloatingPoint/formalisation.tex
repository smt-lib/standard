%!TEX root =  pap.tex

\section{Formalization in Automated Reasoning}
\label{section:formalization}

%The syntax of first-order logic (and many others) is parameterised by
%a \define{signature} $\Sigma$ which gives the functions and predicates (with their
%arities) that may be used in formulae.
%%
%The semantics of first-order logic is formalized in terms of
%\define{interpretations}.  An interpretation $M = (D,i)$ is a set $D$, the
%\define{domain} of discourse, and a map $i$ from $\Sigma$ to functions and
%relations (with the appropriate arities) on $D$ which gives a meaning
%for each symbol in $\Sigma$.
%%
%A formula $\phi$ is \define{valid} if it evaluates to true
%\emph{in all interpretations}.
%%
%`All interpretations' includes those that are nonsensical for a
%particular application.
%%For example, if we wish to reason about
%%floating-point numbers then we don't care if a formula is false when
%%interpreted over the integers.  Nor do we care if it is false when $<$
%%is interpreted to be equality.
%%
%Informally, this can be handled by `knowing' what is meant by the
%symbols in the signature.
%%
%However to automate reasoning we need a way of encoding the intended
%meaning of symbols in the signature.
%%
%There are two different ways of using formalizations to achieve this;
%we will refer to these as \define{axiomatic} and \define{algebraic}.
%
In general, to automate reasoning in a domain $\mathbf D$ of interest
using a formal logic one has to restrict the set of possible interpretations assigned
to the function and predicate symbols used to build formulas 
representing statements over $\mathbf D$.
There are normally two approaches to achieve this, 
one \define{axiomatic} and one \define{algebraic}.

In the axiomatic approach, typical of interactive theorem proving tools,
one first constructs a formula $\psi$
that \define{axiomatizes} $\mathbf D$, i.e., formally describes (some of) 
the properties of the chosen function and predicate symbols.
%
Then, to check that a particular formula $\phi$ is valid in $\mathbf D$
one asks the prover, in essence, to check the logical validity
of the implication $\psi \limplies \phi$.
%
Previous formalizations of floating-point (see Section
\ref{section:related}) have followed this approach.
%
Since the axiomatization $\psi$ is part of the \emph{input},
no specific support for the domain $\mathbf D$ on the prover's side is needed. 
Formalizations of this kind are flexible and easily extensible,
however, the axioms~$\psi$ tend to be voluminous and intricate,
which can limit the performance of many automated techniques.
Another limitation is that certain domains cannot be captured accurately  by 
a relatively small, or even finite, axiomatization in the prover's logic.
In that case, different, approximate axiomatizations may have to be considered
and compared with respect the trade-offs they offer.

In the algebraic approach, typical of SMT solvers, a domain $\mathbf D$ is formalized instead 
by a set of algebraic structures (i.e., models in the chosen logic) 
that interpret the various functions and predicate symbols.
The formalization is used as a \emph{specification} for the prover, and 
the knowledge of what the symbols mean is pre-built into the prover.
An advantage of this is that fast, domain specific procedures 
can be used to reason about $\mathbf D$.
Moreover, in addition to checking for validity in $\mathbf D$, such procedure
are usually also able to generate counter-examples for invalid formulas.
%
Since these formalizations are used as specifications, the key issues are
whether they can be implemented easily and efficiently and how well
the interpretations they describe capture relevant properties of the domain.

We present a formalization of floating-point arithmetic
in the algebraic style, intended as a specification for SMT solvers, and 
make the case that this accurately captures the semantics of the IEEE-754 
standard.
We concentrate on arithmetic aspects, abstracting away more operational ones,
such as exception handling. Also, we only consider the case of binary 
(as opposed to decimal) floating-point arithmetic, as it is more widely used
in practice. 



