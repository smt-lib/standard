%!TEX root = pap.tex
\section{Introduction}

Real values can be represented in a computer in many ways, with various level
of precision: as fixed-point numbers, binary
or decimal floating-point numbers, rationals, arbitrary precision reals,
etc. Due to the wide availability of high-performance hardware and support in
most programming languages,
binary floating-point has become the dominant representation system.
%
\begin{longv}
Aside from some industry-specific exceptions,
most programs that interact with or model the real world, not least those
employed in safety critical applications, rely on binary floating-point arithmetic.
\end{longv}
%
This creates a significant challenge for program analysis tools: accurate
reasoning about the behavior of (numerical) programs is only possible with
bit-accurate reasoning about floating-point arithmetic. 
Many automated verification tools, such as software model checkers,
rely on solvers for Satisfiability Modulo Theories (SMT)~\cite{BarSST-09} as their reasoning engines.
These solvers use specialized, built-in methods to check the satisfiability of formulas
in \emph{background theories} of interest, such as for instance
the theories of integer numbers, arrays, bit vectors and so on.
Reasoning about floating-point numbers accurately in SMT then requires 
the identification of a suitable theory of floating-point arithmetic.
% 
\begin{longv}
Due to significant semantic differences, reasoning in  substitute theories such as the
reals would generally render the solvers unsound and is thus not a satisfactory option.
\end{longv}

In the past, designing a formal theory of floating-point arithmetic would
have been prohibitively complex, as
different manufacturers used different floating-point formats
which varied from the others in significant, structural aspects. 
%
The introduction, in 1985, and subsequent near
universal adoption of the \fpstd\ standard
has considerably improved the situation. However, the standard is
unsatisfactory as a formal theory definition for a number of reasons: 
it is written in natural language; 
it covers various aspects that are not relevant to developing a theory of floating-point%
\begin{longv}
{} (see Section~\ref{section:limitations-and-omissions})%
\end{longv}%
; 
it is lengthy (the 2008 revision has 70 pages) and, most critically
for automated reasoning purposes, 
it does not describe a logical signature and interpretations.

This paper presents the syntax and semantics of 
a logical theory that formalizes floating-point arithmetic 
based on the \fpstd\ standard,
with the goal of facilitating the construction of automated and precise reasoning tools for it.
The models of our theory correspond exactly to conforming
implementations of \fpstd.
While rather general, our formalization was developed with inputs 
from the SMT community to be a reference theory for SMT solver implementors
and users,
and was recently incorporated in the SMT-LIB standard~\cite{BarST-RR-10},
a widely used input/output language for SMT solvers.
\CTComment{Reworded a bit. It was:
``To close the gap between the widely adopted \fpstd\ standard and a formal
floating-point theory, this paper presents the syntax and semantics of 
an extension of the SMT-LIB standard~\cite{BarST-RR-10} to formalize, and reason about, floating-point arithmetic.''
The models of our theory correspond exactly to conforming
implementations of \fpstd.
}
\MBComment{An improvement.  Thanks.}

\begin{shortv}
This paper makes two specific contributions:
\end{shortv}
\begin{longv}
This paper makes three specific contributions:
\end{longv}
\begin{enumerate}
\begin{longv}
\item
  Discusses the challenges of reasoning about floating-point numbers and illustrates 
  its critical uses.
  [Section \ref{section:fp}]
\end{longv}
\item
Presents mathematical structures intended to formally model
binary floating-point arithmetic. 
[Section \ref{section:models}]
\item
Provides a signature for a theory of floating-point arithmetic and
an interpretation of its operators in terms 
of the mathematical structures defined earlier.
[Section \ref{section:syntax}]
\end{enumerate}

