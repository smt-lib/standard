%!TEX root =  pap.tex

\section{From Models to Theory}
\label{section:syntax}

We now formalize a logical theory of floating-point numbers 
based on the structures defined in the previous section.
\begin{shortv}
We use the version of many-sorted logic adopted by the SMT-LIB 2 standard~\cite{BarST-RR-10}.
\end{shortv}
\begin{longv}
We use the version of many-sorted logic adopted by the SMT-LIB 2 standard~\cite{BarST-RR-10}
whose main differences with traditional many-sorted logic is that 
$(i)$ it allows sorts to be denoted by terms instead of just constants and
$(ii)$ it allows sort, function and predicate symbols to be indexed by one or more natural number indices.
For example, possible sorts may be not just constants like \sy{Int} 
but also terms like $\sy{Array}(\sy{Int}, \sy{Real})$ or 
$\sy{BitVec}_n$ for all $n > 0$.

\subsection{Preliminaries}
\end{longv}

A \define{(logical) signature} is consists of
a set of sort symbols (of arity $\geq 0$) and 
a set of function symbols $f$ with an associated \define{rank}, 
a tuple $(S_1, \ldots, S_n,S)$ of sort terms specifying 
the sort of $f$'s arguments, namely, $S_1, \ldots, S_n$, and result, $S$.
Constants are represented by nullary function symbols;
predicate symbols by function symbols whose return sort is a distinguished sort \bools.
Every signature $\Sigma$ is assumed to contain \bools and constants 
\truec and \falsec of that sort, as well as an overloaded symbol 
\sy{=} of rank $(S,S,\bools)$ for each sort $S$, 
for the identity relation over $S$.
Given a set of sorted variables for each of the sorts in $\Sigma$,
well-sorted terms and well-sorted formulas of signature $\Sigma$ are defined as usual.

For every signature $\Sigma$, a \define{$\Sigma$-interpretation} $\str I$ 
\begin{longv}%
is a structure that%
\end{longv}
interprets each sort $S$ in $\Sigma$ as a non-empty set $\M I S$,
each variable $x$ of sort $S$ as an element $\M I x$ of $\M I S$, %$S^{\str I}$
and each function symbol $f$ of rank $(S_1, \ldots, S_n,S)$ as an element $\M I f$
of the (total) function space $\M{I}{S_1} \cross \cdots \cross \M{I}{S_n} \to \M{I}{S}$.
Additionally, $\str I$ interprets \bools as $\Bool = \{\top,\bot\}$ and 
each \sy{=} of rank $(S,S,\bools)$ as the function that maps $(x,y) \in S \cross S$ 
to $\top$ iff $x$ is $y$.
For each sort $S$, $\str I$ induces a mapping $\M{I}{\_}$ from terms 
of sort $S$ to $\M{I}{S}$ as expected.
A satisfaction relation $\models$ between $\Sigma$-interpretations and 
$\Sigma$-formulas is also defined as expected.
A \define{theory} of signature $\Sigma$ as a pair $T = (\Sigma, \mods)$
where $\mods$ is a set of $\Sigma$-interpretations, the \define{models of $T$},
that is closed under variable reassignment.\begin{longv}\footnote{
That is, every $\Sigma$-interpretation that differs from one in $\mods$
only in how it interprets the variables is also in $\mods$.
}
\end{longv}
We say that a $\Sigma$-formula $\varphi$ is 
\define{satisfiable} (resp., \define{unsatisfiable}) \define{in $T$} 
if $\str{I} \models \varphi$ for some (resp., no) $\str{I} \in \mods$.


\subsection{A Theory of Floating-Point Numbers}

In the following we define a theory \fpth of floating-point numbers in the sense above
by specifying a signature \fpsig and a set of \fpmods of \fpsig-interpretations.

The sorts of \fpsig consist, besides \bools, of two individual sorts:
$\sy{RoundingMode}$ and $\sy{Real}$; and two sort families:
$\sy{BitVec}_\nu$, indexed by an integer $\nu > 0$, and 
$\sy{FloatingPoint}_{\ebits,\sbits}$,
indexed by two integers $\ebits,\sbits > 1$. 
The set of function symbols of \fpsig, and their ranks, is given in Table~\ref{table:constructors} through~\ref{table:conversions}.
In those tables, 
we abbreviate $\sy{RoundingMode}$, $\sy{FloatingPoint}$, and $\sy{BitVec}$ 
respectively as $\rms$, $\fps$, and $\bvs$.


We define the set \fpmods as the set of \emph{all possible} 
\fpsig-interpretations $\str I$ 
that interpret sort and function symbol as specified 
in Table~\ref{table:sorts} through~\ref{table:conversions}
in terms of the sets and functions introduced in Section~\ref{section:models}.
Many of the function symbols are \define{overloaded}
for having different ranks;
so we specify their interpretation separately for each rank.


\begin{table}[t]
\caption{Sorts and their interpretation requirements}
\centering
\(
\begin{array}{rcl@{\qquad}rcl}
  \M{I}{\sy{RoundingMode}} & = & \RND &
  \M{I}{\sy{FloatingPoint}_{\ebits,\sbits}} & = & \F{\ebits}{\sbits}
  \\[1.2ex]
  \M{I}{\sy{Real}} & = & \R &
  \M{I}{\sy{BitVec}_\nu} & = & \BV{\nu} 
\end{array}
\)
\label{table:sorts}
\end{table}


\begin{table}[t]
\caption{Constructor symbols and their interpretation}
Symbols of rank $\sy{RM}$:
\[
\begin{array}{rcl@{\qquad}rcl@{\qquad}rcl}
 \M{I}{\sy{rne}} & = & \rne &
 \M{I}{\sy{rna}} & = & \rna &
 \M{I}{\sy{rtp}} & = & \rtp
 \\[1.2ex]
 \M{I}{\sy{rtn}} & = & \rtn &
 \M{I}{\sy{rtz}} & = & \rtz
\end{array}
\]
Symbols of rank $\fpsi$:
\[
\begin{array}{rcl@{\qquad}rcl}
 \M{I}{\sy{\text{+}oo}_{\ebits,\sbits}} & = & (0,\ones{\ebits},\zeros{\sbits-1}) &
 \M{I}{\sy{\text{+}zero}_{\ebits,\sbits}} & = & (0,\zeros{\ebits},\zeros{\sbits-1})
 \\[1.2ex]
 \M{I}{\sy{\text{-}oo}_{\ebits,\sbits}} & = & (1,\ones{\ebits},\zeros{\sbits-1}) &
 \M{I}{\sy{\text{-}zero}_{\ebits,\sbits}} & = & (1,\zeros{\ebits},\zeros{\sbits-1})
 \\[1.2ex]
 \M{I}{\sy{NaN}_{\ebits,\sbits}} & = & \fnan &
\end{array}
\]
Symbols of rank $(\bvs_1,\bvs_\ebits,\bvs_\sbits,\fpsi)$:
\[
\begin{array}{rcl@{\qquad}rcl}
 \M{I}{\sy{fp}} & = & \lambda (b_1,b_\ebits,b_{\sbits-1}).\, \fun{bitpatternToFP}_{\ebits, \sbits} (b_1 \conc b_\ebits \conc b_{\sbits-1})
% \M{I}{\sy{\text{+}zero}} & = & (0,\zeros{\ebits},\zeros{\sbits})
% \\[1.2ex]
% \M{I}{\sy{\text{-}oo}} & = & (1,\ones{\ebits},\zeros{\sbits}) &
% \M{I}{\sy{\text{-}zero}} & = & (1,\zeros{\ebits},\zeros{\sbits})
% \\[1.2ex]
% \M{I}{\sy{NaN}} & = & \fnan &
\end{array}
\]
\label{table:constructors}
\end{table}

As shown in Table~\ref{table:constructors},
the theory has a family of symbols denoting the floating-point infinities, zeros, 
and $\fnan$ for each pair of exponent and significand length.
It also has a ternary function symbol $\sy{fp}$ 
that constructs a floating point number from a triple
of bit vectors respectively storing the sign, exponent and significant.
This allows us to represent all non-$\fnan$ values with bit-level precision.

\begin{table}
\caption{Main symbols and their interpretation}
Symbols of rank $(\fpsi,\fpsi)$:
\[
\begin{array}{rcl@{\qquad}rcl}
 \M{I}{\sy{fp.abs}} & = & \fun{abs}_{\ebits,\sbits} &
 \M{I}{\sy{fp.neg}} & = & \fun{neg}_{\ebits,\sbits}
\end{array}
\]
Symbols of rank $(\fpsi,\fpsi,\fpsi)$:
\[
\begin{array}{rcl@{\qquad}rcl}
 \M{I}{\sy{fp.max}} & = & \fun{max}_{\ebits,\sbits} &
 \M{I}{\sy{fp.min}} & = & \fun{min}_{\ebits,\sbits}
 \\[1.2ex]
 \M{I}{\sy{fp.rem}} & = & \fun{remrne}_{\ebits,\sbits} &
                    &   &                          
\end{array}
\]
Symbols of rank $(\sy{RM},\fpsi,\fpsi)$:
\begin{longv}
\[
\begin{array}{rcl@{\qquad}rcl}
 \M{I}{\sy{fp.sqrt}} & = & \fun{sqrt}_{\ebits,\sbits} &
 \M{I}{\sy{fp.roundToIntegral}} & = & \fun{rti}_{\ebits,\sbits}
\end{array}
\]
\end{longv}
\begin{shortv}
\[
\begin{array}{rcl@{\qquad}rcl}
 \M{I}{\sy{fp.roundToIntegral}} & = & \fun{rti}_{\ebits,\sbits}
\end{array}
\]
\end{shortv}
Symbols of rank $(\sy{RM},\fpsi,\fpsi,\fpsi)$:
\[
\begin{array}{rcl@{\qquad}rcl}
 \M{I}{\sy{fp.add}} & = & \fun{add}_{\ebits,\sbits} &
 \M{I}{\sy{fp.sub}} & = & \fun{sub}_{\ebits,\sbits}
 \\[1.2ex]
 \M{I}{\sy{fp.mul}} & = & \fun{mul}_{\ebits,\sbits} &
 \M{I}{\sy{fp.div}} & = & \fun{div}_{\ebits,\sbits}
% \\[1.2ex]
\end{array}
\]
Symbols of rank $(\sy{RM},\fpsi,\fpsi,\fpsi,\fpsi)$:
\[
\begin{array}{rcl@{\qquad}rcl}
 \M{I}{\sy{fp.fma}} & = & \fun{fma}_{\ebits,\sbits}
\end{array}
\]
Symbols of rank $(\fpsi,\bools)$:
\[
\begin{array}{rcl@{\quad}rcl}
 \M{I}{\sy{fp.isNormal}} & = & \FBVN{\ebits}{\sbits} &
 \M{I}{\sy{fp.isNegative}} & = & \fun{isNeg}_{\ebits,\sbits}
 \\[1.2ex]
 \M{I}{\sy{fp.isSubnormal}} & = & \FBVS{\ebits}{\sbits} &
 \M{I}{\sy{fp.isPositive}} & = & \fun{isPos}_{\ebits,\sbits}
 \\[1.2ex]
 \M{I}{\sy{fp.isInfinite}} & = & \FBVI{\ebits}{\sbits} &
 \M{I}{\sy{fp.isZero}} & = & \FBVZ{\ebits}{\sbits}
 \\[1.2ex]
 \M{I}{\sy{fp.isNaN}} & = & \set{\fnan}
\end{array}
\]
Symbols of rank $(\fpsi,\fpsi,\bools)$,
where $\fun{gt}_{\ebits,\sbits}$ is the converse of $\fun{lt}_{\ebits,\sbits}$:
\[
\begin{array}{rcl@{\qquad}rcl}
 \M{I}{\sy{fp.lt}} & = & \fun{lt}_{\ebits,\sbits} &
 \M{I}{\sy{fp.leq}} & = & \fun{leq}_{\ebits,\sbits}
 \\[1.2ex]
 \M{I}{\sy{fp.gt}} & = & \fun{gt}_{\ebits,\sbits} &
 \M{I}{\sy{fp.geq}} & = & \fun{geq}_{\ebits,\sbits}
 \\[1.2ex]
 \M{I}{\sy{fp.eq}} & = & \fun{eq}_{\ebits,\sbits}
\end{array}
\]
\label{table:functions}
\end{table}


Table~\ref{table:functions} lists function symbols for the various arithmetic
operations over floating-point numbers, and provides their semantics in terms
of the operations defined in Subsection~\ref{section:operations}.
%
The table lists predicate symbols
corresponding to the relations defined in Subsection~\ref{section:relations} and 
to the various subsets of $\F{\ebits}{\sbits}$.
For simplicity and by a slight abuse of notation, we identify functions $h$ of type
$D_1 \cross \cdots \cross D_n \to \Bool$ with the $n$-ary relations
$\setst{(x_1, \ldots, x_n) \in D_1 \cross \cdots \cross D_n}{h(x_1, \ldots, x_n) = \top}$.


%\begin{table}
%\caption{Predicate symbols and their interpretation}
%Symbols of rank $(\fpsi,\bools)$:
%\[
%\begin{array}{rcl@{\quad}rcl}
% \M{I}{\sy{fp.isNormal}} & = & \FBVN{\ebits}{\sbits} &
% \M{I}{\sy{fp.isNegative}} & = & \fun{isNeg}_{\ebits,\sbits}
% \\[1.2ex]
% \M{I}{\sy{fp.isSubnormal}} & = & \FBVS{\ebits}{\sbits} &
% \M{I}{\sy{fp.isPositive}} & = & \fun{isPos}_{\ebits,\sbits}
% \\[1.2ex]
% \M{I}{\sy{fp.isInfinite}} & = & \FBVI{\ebits}{\sbits} &
% \M{I}{\sy{fp.isZero}} & = & \FBVZ{\ebits}{\sbits}
% \\[1.2ex]
% \M{I}{\sy{fp.isNaN}} & = & \set{\fnan}
%\end{array}
%\]
%Symbols of rank $(\fpsi,\fpsi,\bools)$,
%where $\fun{gt}_{\ebits,\sbits}$ is the converse of $\fun{lt}_{\ebits,\sbits}$:
%\[
%\begin{array}{rcl@{\qquad}rcl}
% \M{I}{\sy{fp.lt}} & = & \fun{lt}_{\ebits,\sbits} &
% \M{I}{\sy{fp.leq}} & = & \fun{leq}_{\ebits,\sbits}
% \\[1.2ex]
% \M{I}{\sy{fp.gt}} & = & \fun{gt}_{\ebits,\sbits} &
% \M{I}{\sy{fp.geq}} & = & \fun{geq}_{\ebits,\sbits}
% \\[1.2ex]
% \M{I}{\sy{fp.eq}} & = & \fun{eq}_{\ebits,\sbits}
%\end{array}
%\]
%\label{table:predicates}
%\end{table}



\begin{table}[t]
\caption{Conversion symbols and their interpretation}
Conversions to floating-point
\[
\begin{array}{rcl}
 \M{I}{\sy{to\_fp_{\ebits,\sbits}}:(\rms,\fps_{\ebits',\sbits'},\fpsi)} & = & \fun{cast}_{\ebits', \sbits',\ebits,\sbits}
 \\[1.2ex]
 \M{I}{\sy{to\_fp_{\ebits,\sbits}}:(\bvs_{\ebits+\sbits},\fpsi)} & = & \fun{bitpatternToFP}_{\ebits, \sbits}
 \\[1.2ex]
 \M{I}{\sy{to\_fp_{\ebits,\sbits}}:(\rms,\sy{Real},\fpsi)} & = & \fun{realToFP}_{\ebits,\sbits}
 \\[1.2ex]
 \M{I}{\sy{to\_fp_{\ebits,\sbits}}:(\rms,\bvs_\nu,\fpsi)} & = & \fun{sIntToFP}_{\nu,\ebits,\sbits}
 \\[1.2ex]
 \M{I}{\sy{to\_fp\_unsigned_{\ebits,\sbits}}:(\rms,\bvs_\nu,\fpsi)} & = & \fun{uIntToFP}_{\nu,\ebits,\sbits}
\end{array}
\]
Conversions from floating-point
\[
\begin{array}{rcl}
 \M{I}{\sy{fp.to\_sbv_\nu}:(\fpsi,\bvs_\nu)} & = & \fun{FPToSInt}_{\nu,\ebits,\sbits}
 \\[1.2ex]
 \M{I}{\sy{fp.to\_ubv_\nu}:(\fpsi,\bvs_\nu)} & = & \fun{FPToUInt}_{\nu,\ebits,\sbits}
 \\[1.2ex]
 \M{I}{\sy{fp.to\_real}:(\fpsi,\sy{Real})} & = & \fun{FPToReal}_{\nu,\ebits,\sbits}
\end{array}
\]
\label{table:conversions}
\end{table}


Finally, Table~\ref{table:conversions} lists function symbols
corresponding to the various conversion functions introduced in Subsection~\ref{section:conversions} as well as the casting function between floating-point sets 
of different precision.

%
%A set of parametric value functions allow users to easily construct
%literal values.  Bit vectors are required to specify normal and
%subnormal numbers to avoid the complexity (and potential error) of
%converting from decimal strings.  Let $con$ denote a function that
%concatinates all of its bit vector arguments.
%%
%\begin{eqnarray*}
%  \pintr{fp}{\BV{1},\BV{\ebits},\BV{i}} & = &
%  bitpatternToFloat_{\ebits, i - 1} \fcompose con \\
%  \intr{(\_ +oo e s)} & = & (0,\ones{\ebits},\zeros{\sbits}) \in \F{\ebits}{\sbits}\\
%  \intr{(\_ -oo e s)} & = & (1,\ones{\ebits},\zeros{\sbits}) \in \F{\ebits}{\sbits}\\
%  \intr{(\_ +zero e s)} & = & (0,\zeros{\ebits},\zeros{\sbits}) \in \F{\ebits}{\sbits}\\
%  \intr{(\_ -zero e s)} & = & (1,\zeros{\ebits},\zeros{\sbits}) \in \F{\ebits}{\sbits}\\
%  \intr{(\_ NaN e s)} & = & \fnan \in \F{\ebits}{\sbits}
%\end{eqnarray*}
%
%\MBComment{Cesare : there is a clash here with the notes in the theory
%definition}
%\MBComment{Martin to reconcile these.}
%
%The function symbols in the theory map directly on to the operators
%previously defined on $\F{\ebits}{\sbits}$.  As these symbols are polymorphic,
%the values of $eb$ and $sb$ will come from their usage.
%\MBComment{Suggestions for how to represent the polymorphism of these
%  functions in terms of $\intr{.}$ would be appreciated.}
%\MBComment{Is this OK?}
%%
%\begin{eqnarray*}
%  \pintr{fp.abs}{\F{\ebits}{\sbits}} & = & abs_{\ebits,\sbits} \\
%  \pintr{fp.neg}{\F{\ebits}{\sbits}} & = & neg_{\ebits,\sbits} \\
%  \pintr{fp.add}{\F{\ebits}{\sbits}} & = & add_{\ebits,\sbits} \\
%  \pintr{fp.sub}{\F{\ebits}{\sbits}} & = & sub_{\ebits,\sbits} \\
%  \pintr{fp.mul}{\F{\ebits}{\sbits}} & = & mul_{\ebits,\sbits} \\
%  \pintr{fp.div}{\F{\ebits}{\sbits}} & = & div_{\ebits,\sbits} \\
%  \pintr{fp.fma}{\F{\ebits}{\sbits}} & = & fma_{\ebits,\sbits} \\
%  \pintr{fp.sqrt}{\F{\ebits}{\sbits}} & = & sqrt_{\ebits,\sbits} \\
%  \pintr{fp.rem}{\F{\ebits}{\sbits}} & = & rem_{\ebits,\sbits} \\
%  \pintr{fp.roundToIntegral}{\F{\ebits}{\sbits}} & = & rti_{\ebits,\sbits} \\
%  \pintr{fp.min}{\F{\ebits}{\sbits}} & = & min_{\ebits,\sbits} \\
%  \pintr{fp.max}{\F{\ebits}{\sbits}} & = & max_{\ebits,\sbits}
%\end{eqnarray*}
%
%Relation symbols in the theory are interpreted similarly:
%%
%\begin{eqnarray*}
%  \reversepairs{Y} & = & \setst{(g,f)}{(f,g) \in Y} \\
%  \pintr{fp.leq}{\F{\ebits}{\sbits}} & = & \mathit{eq}_{\ebits,\sbits} \union \mathit{lt}_{\ebits,\sbits} \\
%  \pintr{fp.lt}{\F{\ebits}{\sbits}} & = & \mathit{lt}_{\ebits,\sbits} \\
%  \pintr{fp.geq}{\F{\ebits}{\sbits}} & = & \reversepairs{(\mathit{eq}_{\ebits,\sbits} \union \mathit{lt}_{\ebits,\sbits})} \\
%  \pintr{fp.gt}{\F{\ebits}{\sbits}} & = & \reversepairs{\mathit{lt}_{\ebits,\sbits}} \\
%  \pintr{fp.eq}{\F{\ebits}{\sbits}} & = & \mathit{eq}_{\ebits,\sbits} \\
%  \pintr{fp.isNormal}{\F{\ebits}{\sbits}} & = & \FBVN{\ebits}{\sbits} \\
%  \pintr{fp.isSubnormal}{\F{\ebits}{\sbits}} & = & \FBVS{\ebits}{\sbits} \\
%  \pintr{fp.isZero}{\F{\ebits}{\sbits}} & = & \FBVZ{\ebits}{\sbits} \\
%  \pintr{fp.isInfinite}{\F{\ebits}{\sbits}} & = & \FBVI{\ebits}{\sbits} \\
%  \pintr{fp.isNaN}{\F{\ebits}{\sbits}} & = & \set{\fnan} \\
%  \pintr{fp.isNegative}{\F{\ebits}{\sbits}} & = & \mathit{isNeg}_{\ebits,\sbits} \\
%  \pintr{fp.isPositive}{\F{\ebits}{\sbits}} & = & \mathit{isPos}_{\ebits,\sbits}
%\end{eqnarray*}
%
%Finally, function symbols for conversions between sorts are
%interpreted in a similar fashion.  Note that these symbols are both
%indexed and polymorphic in some cases.
%%
%\begin{eqnarray*}
%  \pintr{(\_ to\_fp e s)}{\BV{e + s}} & = & bitpatternToFloat_{\ebits, \sbits}\\
%  \pintr{(\_ to\_fp e s)}{\F{\mbits}{\nbits}} & = & cast_{\mbits,\nbits,\ebits,\sbits}\\
%  \pintr{(\_ to\_fp e s)}{\R} & = & realToFloat_{\ebits, \sbits}\\
%  \pintr{(\_ to\_fp e s)}{\BV{n}} & = & signedIntegerToFloat_{\BV{n},\F{\ebits}{\sbits}}\\
%  \pintr{(\_ to\_fp\_unsigned e s)}{\BV{n}} & = & unsignedIntegerToFloat_{\BV{n},\F{\ebits}{\sbits}}\\
%  \pintr{(\_ fp.to\_ubv m)}{\F{\ebits}{\sbits}} & = & floatToUnsignedInteger_{\BV{n},\F{\ebits}{\sbits}}\\
%  \pintr{(\_ fp.to\_sbv m)}{\F{\ebits}{\sbits}} & = & floatToSignedInteger_{\BV{n},\F{\ebits}{\sbits}}\\
%  \pintr{fp.to\_real}{\F{\ebits}{\sbits}} & = & floatToReal_{\ebits, \sbits} 
%\end{eqnarray*}
%
%
%% Say the theory is all of the things that are true for any of these interpretations.
%
%This allows us to define the theory of floating-point:
%
%\MBComment{Cesare to rework to match the SMT-LIB definition of a theory.}
%
%\begin{definition}
%  Let $F_{fp}$ be the set of first-order formulae that can be
%  constructed from signature $\sig{fp}$ and let $M$ be the
%  set of interpretations of the form described above.  Thus we define
%  \emph{the theory of floating-point}, $\theoryof{fp}$ as:
%
%  \begin{equation*}
%    \theoryof{fp} = \setst{\phi \in F_{fp}}
%                          {\forall m \in M . m \models \phi}
%  \end{equation*}
%\end{definition}
%
