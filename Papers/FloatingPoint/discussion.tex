%!TEX root =  pap.tex

\begin{longv}
\section{Discussion}
\label{section:discussion}

This work originated in the context of the SMT-LIB initiative\footnote{
See \url{www.smt-lib.org} .
}
with the goal of defining a standard, reference SMT-LIB theory 
of floating-point numbers for SMT solvers.
The process of defining this reference theory was guided 
by three, somewhat conflicting, main principles:

\begin{enumerate}
\item{minimise the amount of effort required to implement a solver for
  such a theory;}
\item{support a wide range of applications
(including avoiding hardware or language specific idioms);}
\item{conform strictly with \fpstd in the sense that 
any compliant implementation of \fpstd\ 
would form a model of the theory and vice versa.}
\end{enumerate}


\subsection{Process of Standardisation}

An initial proposal for an SMT-LIB floating-point theory was made
by P.~R{\"u}mmer and T.~Wahl in 2010~\cite{RW10}.  A second draft
based on that proposal was produced by C.~Tinelli 
using initial community feedback.  This
draft was used by a number of system implementors to produce
benchmarks and theory solvers.  To draw on the experience of using the
theory, a work group was formed to decide on a first official
theory definition.  The suggestions made and changes requested by the
work group were written up by C.~Tinelli and M.~Brain 
and released to the SMT-LIB community mailing list.  
We used feedback and corrections from this final review to prepare 
the version of the theory presented here.
The semantics we developed for this theory,
discussed in Section~\ref{section:formal-foundations}, was inspired
by the initial work in~\cite{RW10}, but
differs substantially from that work by providing
a bit-precise semantics of \fpstd\ floating-point.

\enlargethispage*{2ex}

\subsection{Limitations and Omissions}
\label{section:limitations-and-omissions}


The theory presented here does not cover all of the
functionality of \fpstd.  
Here is a list of salient omissions and restrictions and their rationale.

\begin{itemize}
\item
Two radices, 2 and 10, are included in \fpstd, although compliant
implementations are not required to support both.  Our theory only 
covers radix 2 (binary) representations, as
decimal representations are not widely implemented,
supported by languages, or used.  

\item{\fpstd\ gives four ``specification levels'' (extended real, floating-point
  data, representations and bit-strings) with maps between them.  The
  formalization in this paper is based on the second level of specification.
  Thus there is only one \fnan, as quiet and signalling \fnan\ are
  introduced at level three.  Also, \fnan\ is not regarded to have a
  sign or a payload as these concepts are introduced at level four.
  As a consequence, functionality that uses these properties of \fnan,
  such as \emph{copy sign}, \emph{sign of} and the \emph{total order} predicate, has
  been omitted.}

\item
Our formalization does not cover the \fpstd\ notion 
of exceptions or flags (the default handling of executions).
This is simply because there is no notion of execution order in a logical formula, 
and so there is no meaningful way of expressing those notions directly
in theory.

\item
The 2008 revision of \fpstd\ adds the notion of attributes, 
a means of attaching implicit parameters to sections of programs.  
Only the most commonly supported attribute, rounding direction, is 
modeled in our theory---by adding an explicit rounding mode parameter
to operations affected by it.  
Some of the other attributes could be effectively modeled but we chose
not to do so because they are programming language specific.

\item
We do not support unordered variants of comparison operators 
(those that return true if one or more of the arguments are $\fnan$)
since they are rarely used. 
It would, however, be relatively simple to model these variants.

\item
\fpstd\ contains recommendations for trigonometric functions
and exponentials but neither are mandated.  
The accuracy of implementations of these functions vary significantly, 
making it very hard to come up with logical models that are widely 
applicable but also meaningfully constrained.
Similarly, there are only recommendations for reduction
operations, and implementations vary.
\end{itemize}


\end{longv}

%\subsection{Underspecified Functions}
%\label{section:underspecified}
%
%The standard contains some areas which are language or implementation
%defined for a variety of reasons (to allow a wider range of compliant
%interpretations, for speed / performance, to allow languages to add
%their own rules).  Unlike C undefined is not necessarily wrong.  Use
%partial specification (of total functions / relations) to handle
%these.
%
%Thus a satisfying model might not correspond to a real execution in
%some programming languages / on some hardware.  If you want this you
%will have to axiomatise the behaviour of the particular platform or
%language.  Note that this may be harder than it first appears -- for
%example x87 and SSE differ so the exact behaviour may be dependent on
%compiler flags.
%




